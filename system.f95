

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

                                                        MODULE ALLOCMOD


    !PUBLIC
    CHARACTER(LEN=5):: VERSION='V20.6'

    INTEGER i,ALLCOMPANIES,NDAYS,BEGIN,MAXPARTITION,MAXHISTORY,HISTORYSTEP,MAXSEG,MAXHOLDING,MAXSEARCH,&
    MAXPARTS,MAXTDAYS,SEARCH,EXCHANGE,TOTALCOUNTDAYS,TAU,SEG,PARTITION,HISTORY,EPSILON,HOLDINGDAYS,HOLDING,l,k,&
    MAXEPSILON,MAXTAU,MINTAU,MINEPSILON,MAXMEMORY,MAXORDER,MINDELTA,MAXDELTA,DELTA

    REAL AVGNDAYS,AVGMONEY,AVGPRICE,MINAVGPRICE,MINCAP, MINSTAT

    INTEGER :: COUNT1,COUNT2,COUNT3,COUNT4,COUNT5,COUNT6,COUNT7,COUNT_RATE, COUNT_MAX

    INTEGER,DIMENSION(3):: TODAY,NOW,CLOSEDATE,CALENDARTODAY

    CHARACTER(LEN=3)::DAYNAME
    CHARACTER(LEN=3)::COUNTRY
    CHARACTER(LEN=15)::XPREDICTIONS
    CHARACTER(LEN=13)::XOBJECTIVE
    CHARACTER(LEN=19)::XPERFORMANCE
    CHARACTER(LEN=11)::DATEFOLDER
    CHARACTER(LEN=12)::COMPANY
    CHARACTER(LEN=1)::FIRSTLETTER
    CHARACTER(LEN=11)::LASTLETTER
    CHARACTER(LEN=3)::STOCKEXCHANGE
    CHARACTER(LEN=2)::AGENT_ID

    INTEGER,PARAMETER::list1=2000
    INTEGER,PARAMETER::list2=20000
    INTEGER,PARAMETER::list3=40000

    INTEGER,DIMENSION(:,:),ALLOCATABLE::PRIME
    CHARACTER(LEN=4),DIMENSION(:,:),ALLOCATABLE::ACTION

    CHARACTER(len=15),DIMENSION(:),ALLOCATABLE::CO
    INTEGER,DIMENSION(:),ALLOCATABLE::CODATE
    REAL(4) ,DIMENSION(:,:), ALLOCATABLE::DPV,WDPV
    REAL(4) ,DIMENSION(:,:,:), ALLOCATABLE::RSI!,MFI
    REAL,DIMENSION(:,:),ALLOCATABLE::OBVI,MAOZ!,STOX
    REAL(4) ,DIMENSION(:,:,:),ALLOCATABLE::MA
    REAL,DIMENSION(:),ALLOCATABLE::GP
    INTEGER,DIMENSION(:,:,:),ALLOCATABLE::PATTERNBACKDATE
    REAL,DIMENSION(:,:,:,:),ALLOCATABLE::MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,&
    MAXMAOZAVG,MINMAOZAVG!,MAXSTOXAVG,MINSTOXAVG,MAXMFIAVG,MINMFIAVG
    REAL,DIMENSION(:,:,:),ALLOCATABLE::RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL
!    REAL(4),DIMENSION(-20:20)::PBUYEXEC,PSELLEXEC
!    REAL(4), DIMENSION(:,:), ALLOCATABLE::SIGMA

    REAL(4),DIMENSION(:,:,:,:,:,:,:),ALLOCATABLE::NETGAINA1,NETLOSSA1,GAINERSA1,LOSERSA1,&
    NETGAINA3,NETLOSSA3,GAINERSA3,LOSERSA3,PATHCOSTA1,PATHCOSTA3,PATHPROFITA1,PATHPROFITA3,&
    PATHPROFITCOUNTA1,PATHPROFITCOUNTA3,PATHCOSTCOUNTA1,PATHCOSTCOUNTA3,MASTERA1,MASTERA3

    REAL,DIMENSION(:,:),ALLOCATABLE::CALENDARRETURNS
    REAL,DIMENSION(:,:),ALLOCATABLE::CALENDARLOSS

    REAL,DIMENSION(:),ALLOCATABLE::S1,S2,S3,S4
    REAL,DIMENSION(:),ALLOCATABLE::S1_AVG,S2_AVG,S3_AVG,S4_AVG
    REAL,DIMENSION(:),ALLOCATABLE::GRATIO_AVG,NETGAIN_AVG,NETLOSS_AVG,GRATIO,NETGAIN,NETLOSS






                                                            END MODULE
!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------









!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE ZELLERMOD

                                                        CONTAINS


!------------------------------------------------------------------------------------------------------------------------------------

                                    SUBROUTINE ZELLER(CLOSEDATE,DAYNAME)

    IMPLICIT NONE

!In Zeller’s algorithm, the months are numbered from 3 for March to 14 for February.
!The year is assumed to begin in March; this means, for example, that January 1995 is to be treated as month 13 of 1994.
!-------------------------------------------------------------
    INTEGER,DIMENSION(3),INTENT(IN)::CLOSEDATE
    CHARACTER(LEN=3),INTENT(OUT)::DAYNAME


    INTEGER c,y,m,d
    INTEGER::w
    CHARACTER(LEN=3),DIMENSION(0:6)::DAYOFWEEK

!-------------------------------------------------------------
!GENERATE DEFINITIONS

    DAYOFWEEK= [ 'SUN','MON','TUE','WED','THU','FRI','SAT' ]

    D=CLOSEDATE(1)
    M=CLOSEDATE(2)
    Y=2000-CLOSEDATE(3)
    C=20
!-------------------------------------------------------------
!write(*,*) c,y,m,d
!-------------------------------------------------------------
!The formula for the Gregorian calendar is

    IF (M < 3 ) THEN
    M=M+12; Y=Y-1
    ENDIF

!    W = mod(REAL( d + (13*(m+1)/5.0) +y +(y/4.0) +(c/4.0) - 2*c ) ,7.0)
    W = INT(mod( D + INT(13*(M+1)/REAL(5)) + Y +INT(Y/REAL(4)) + INT(C/REAL(4)) +5*C, 7))
!-------------------------------------------------------------
!    IF (W < 0) THEN
!    W=W+7
!    ENDIF

    DAYNAME=DAYOFWEEK(W)

!-------------------------------------------------------------

!Y is the year minus 1 for  January or February, and the year for  the rest of the year
!y is the last 2 digits of Y
!c is the first 2 digits of Y
!d is the day of the month (1 to 31)
!m is the shifted month (March=3,...February=14)
!w is the day of week (1=Sunday,..0=Saturday)


!------------------------------------------------------------------------------------------------------------------------------------

                                            END SUBROUTINE
                                            END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------




!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE DATEMOD

                                                        CONTAINS

                                                SUBROUTINE DATE(TODAY,NOW)
    IMPLICIT NONE

    INTEGER,DIMENSION(3),INTENT(OUT):: TODAY, NOW
    1000 FORMAT('DATE ', I2.2, '/', I2.2, '/', I4.4, ' TIME ',I2.2, ':', I2.2, ':', I2.2)


    CALL IDATE(TODAY)   ! TODAY(1)=DAY, (2)=MONTH, (3)=YEAR
    CALL ITIME(NOW)     ! NOW(1)=HOUR, (2)=MINUTE, (3)=SECOND

    WRITE (*,1000)  TODAY(2), TODAY(1), TODAY(3), NOW


                                                    END SUBROUTINE DATE
                                                        END MODULE
!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------











!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE SUBROUTINES


                                                        CONTAINS



!------------------------------------------------------------------------------------------------------------------------------------



                                        SUBROUTINE TODAYFOLDER(DATEFOLDER,TODAY)

    IMPLICIT NONE

    INTEGER,DIMENSION(3),INTENT(OUT):: TODAY
    INTEGER,DIMENSION(3)::NOW

    CHARACTER(LEN=11),INTENT(OUT)::DATEFOLDER

    CALL ITIME(NOW) ! NOW(1)=HOUR, (2)=MINUTE, (3)=SECOND

    CALL IDATE(TODAY) ! TODAY(1)=DAY, (2)=MONTH, (3)=YEAR

    WRITE(DATEFOLDER,'(I2,A1,I2,A1,I4)') today(2),'-',today(1),'-',today(3)



                                                END SUBROUTINE


!------------------------------------------------------------------------------------------------------------------------------------


                                            SUBROUTINE MAILINGFORECAST(COUNTRY)


    CHARACTER(LEN=3),INTENT(in)::COUNTRY
    INTEGER,DIMENSION(3)::TODAY
    CHARACTER(LEN=4),DIMENSION(1:12)::MONTH

    !GENERATE DEFINITIONS
     MONTH(1:12)= [ 'Jan ','Feb ','Mar ','Apr ','May ','June','July','Aug ','Sep ','Oct ','Nov ','Dec ' ]

    CALL iDATE(TODAY)


    WRITE(100001,*) '\tabcolsep=0.11cm'
    WRITE(100001,*) '\begin{longtable}{clclclclclclclclclc}'
    WRITE(100001,*) '\multicolumn{8}{c} { {\sc Release date:} {\sc ',MONTH(today(2)),'}',' ',today(1),',',today(3),'} \\ '
    WRITE(100001,*) '          & Trade     & Ticker  & Last  &Probability  &Holding  &Closed at & \\'
    WRITE(100001,*) 'Signal on: & direction & symbol  & price  &of success  &period  & price & Return (\%)\\ \toprule '
    WRITE(100001,*) '\endfirsthead'

    WRITE(100001,*) '          & Trade     & Ticker  & Last  &Probability  &Holding  &Closed at & \\'
    WRITE(100001,*) 'Signal on: & direction & symbol  & price  &of success  &period  & price & Return (\%)\\ \toprule '
    WRITE(100001,*) '\endhead'


                                            END SUBROUTINE


!------------------------------------------------------------------------------------------------------------------------------------



                                    SUBROUTINE LISTGEN(ALLCOMPANIES,CO,EXCHANGE,COUNTRY,XPREDICTIONS,XOBJECTIVE, &
                                                        XPERFORMANCE)


    CHARACTER(len=15),DIMENSION(:),INTENT(OUT),ALLOCATABLE::CO
    INTEGER,INTENT(OUT)::ALLCOMPANIES
    CHARACTER(LEN=3),INTENT(OUT)::COUNTRY
    CHARACTER(LEN=15),INTENT(OUT)::XPREDICTIONS
    CHARACTER(LEN=13),INTENT(OUT)::XOBJECTIVE
    CHARACTER(LEN=19),INTENT(OUT)::XPERFORMANCE
    INTEGER,INTENT(IN)::EXCHANGE
    INTEGER i

    ALLCOMPANIES=0

    SELECT CASE(EXCHANGE)
    !-----------------------------------------------------------------------
    CASE(1);    COUNTRY='AME'
    !-----------------------------------------------------------------------
    CASE(2);    COUNTRY='CDA'
    !-----------------------------------------------------------------------
    CASE(3);    COUNTRY='EUR'
    !-----------------------------------------------------------------------
    CASE(4);    COUNTRY='A&O'
    !-----------------------------------------------------------------------
    CASE(5);    COUNTRY='MEX'
    !-----------------------------------------------------------------------

    END SELECT

    XPREDICTIONS=COUNTRY
    XOBJECTIVE=COUNTRY
    XPERFORMANCE=COUNTRY

    OPEN(UNIT=100,FILE='/GeTeX/system/EXCHANGES/'//COUNTRY,STATUS='old')

    DO; READ(100,*,END=1001); ALLCOMPANIES=ALLCOMPANIES+1; ENDDO
    1001 CONTINUE
    ALLOCATE(CO(ALLCOMPANIES)); REWIND(100)

    DO i=1,ALLCOMPANIES; READ(100,*) CO(i); ENDDO



                                                            END SUBROUTINE


!------------------------------------------------------------------------------------------------------------------------------------



                                                            SUBROUTINE closefile(i)

    INTEGER,INTENT(IN)::i
    INTEGER,PARAMETER::list1=2000
    INTEGER,PARAMETER::list2=20000
    INTEGER,PARAMETER::list3=40000


    close(i+list1)
    close(i+list2)
    close(i+list3)

                                                            END SUBROUTINE


!------------------------------------------------------------------------------------------------------------------------------------




                                            SUBROUTINE INTERNALDATAFEED(i,NDAYS,WDPV,CODATE,BEGIN)

    IMPLICIT NONE

    !POSTION READ AT PENULTIMATE ROW, AT THE BEGGINING OF LAST ROW BUT WITHOUT READING IT.
    INTEGER,INTENT(IN):: i,NDAYS,BEGIN!,GOBACK
    REAL(4) ,DIMENSION(0:NDAYS,1:6)::DPV
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(OUT)::WDPV
    INTEGER,DIMENSION(0:NDAYS),INTENT(OUT)::CODATE
    INTEGER,PARAMETER::list1=2000

    INTEGER l
    REAL VolMax
    REAL,DIMENSION(4)::PriceMax

    !------------------V2.9-------------------------
    DO l=BEGIN,NDAYS-1,1
    READ(i+list1,*)
    ENDDO
    !Begin data inversion
    DO l=BEGIN,NDAYS,1
    READ(i+list1,'(I8,4(F8.2),F9.0)') CODATE(L),DPV(l,2:6)
    BACKSPACE(i+list1)
    READ(i+list1,*) DPV(l,1:6)
    BACKSPACE(i+list1); BACKSPACE(i+list1)
    ENDDO
!---------- Zero element
    DPV(0,1:6)=DPV(1,1:6)
!-----------------------------
!    DO l=1,NDAYS,1
!    WRITE(i+list3,*) DPV(l,5)
!    ENDDO
!-----------------------------
    REWIND(i+list1)
!-----------------------------
    VolMax=MAXVAL(DPV(1:NDAYS,6))
    PriceMax(1:4)=MAXVAL(DPV(1:NDAYS,2:5))
!-----------------------------
!    DO l=1,NDAYS,1
!    NPrice(l,1:4)=DPV(l,2:5)/PriceMax(1:4)
!    IDVolume(l,1:4)= DPV(l,6) * NPrice(l,1:4)
!    INVolume(l,1:4)=ABS((IDVolume(l,1:4)/VolMax) -1)
!    ENDDO

    DO l=1,NDAYS,1
!    WDPV(l,2:5)=DPV(l,2:5) * INVolume(l,1:4)
!    WDPV(l,2:5)=( (DPV(l,2:5)/PriceMax(1:4)) * (DPV(l,6)/VolMax) ) !MONEY OR VALUE CHANGE IN PERCENTAGE
    WDPV(l,2:5)=( (DPV(l,2:5)/PriceMax(1:4)) * (DPV(l,6)/VolMax) - 1.0 ) !ORIGINAL
    WDPV(l,2:5)= DPV(l,2:5) * ABS(WDPV(l,2:5)) !ORIGINAL
!    WDPV(l,2:5)=DPV(l,2:5) * (-WDPV(l,2:5))
    ENDDO


                                                END SUBROUTINE INTERNALDATAFEED



!------------------------------------------------------------------------------------------------------------------------------------



                                        SUBROUTINE EXTERNALDATAFEED(i,NDAYS,DPV,CODATE,BEGIN)

    IMPLICIT NONE

    !POSTION READ AT PENULTIMATE ROW, AT THE BEGGINING OF LAST ROW BUT WITHOUT READING IT.
    INTEGER,INTENT(IN)::i,NDAYS,BEGIN!,GOBACK
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(OUT)::DPV
    INTEGER,DIMENSION(0:NDAYS),INTENT(OUT)::CODATE
    INTEGER l!,K
    INTEGER,PARAMETER::list1=2000

    !------------------V2.9-------------------------
    DO l=BEGIN,NDAYS-1,1
    READ(i+list1,*)
    ENDDO
    !Begin data inversion
    DO l=BEGIN,NDAYS,1
!    READ(i+list1,'(F8.0,4(F8.2),F9.0)',REC=NDAYS-L,ADVANCE='NO') DPV(l,1:6)
!    READ(i+list1,*) CODATE(l)

    READ(i+list1,'(I8,4(F8.2),F9.0)') CODATE(L),DPV(l,2:6)
    BACKSPACE(i+list1)
    READ(i+list1,*) DPV(l,1:6)

    BACKSPACE(i+list1); BACKSPACE(i+list1)
    ENDDO
    !---------- Zero element

    DPV(0,1:6)=DPV(1,1:6)

!    DO K=2,4
!    DO l=BEGIN,NDAYS,1
!    DPV(l,K)=LOG(DPV(l,K))
!    ENDDO
!    ENDDO

    REWIND(i+list1)

    !>>>>>>>>>
 !       DO l=1,NDAYS
 !       WRITE(*,'(a10,1x,i8,6(f12.2),i5)') CO(i),CODATE(L),DPV(l,1:6),L
 !       ENDDO
    !>>>>>>>>>

                                                            END SUBROUTINE




!------------------------------------------------------------------------------------------------------------------------------------


                                                                SUBROUTINE counter(i,NDAYS)
    IMPLICIT NONE

    INTEGER,INTENT(IN)::i
    INTEGER,INTENT(OUT):: NDAYS
    INTEGER,PARAMETER::list1=2000

    !Count total number of data observations. Correct as is.
    REWIND(i+list1); NDAYS=0;DO; READ(i+list1,*,END=100);NDAYS=NDAYS+1;ENDDO
    100 REWIND(i+list1)
                                                                END SUBROUTINE



    !------------------------------------------------------------------------------------------------------------------------------------





                                                SUBROUTINE SMAV(NDAYS,DPV,MA,BEGIN)

    IMPLICIT NONE

    INTEGER, INTENT(IN)::NDAYS,BEGIN
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL(4) ,DIMENSION(2:200,0:NDAYS,1:4),INTENT(OUT)::MA
    INTEGER l,MU


    DO MU=2,200
    ! Calculate the simple moving average, period tau.
    DO l=BEGIN+MU,NDAYS
    MA(MU,l,1:4)=[ SUM(DPV(l-MU:l,2:5) ,dim=1) /REAL(MU+1) ]
!    WRITE(*,*) DPV(L,1:4)
!    WRITE(*,*) MA(MU,L,1:4)
    END DO
    ENDDO

                                                        END SUBROUTINE



    !------------------------------------------------------------------------------------------------------------------------------------




                                                    SUBROUTINE EXPAV(NDAYS,DPV,MA,BEGIN)
    IMPLICIT NONE

    INTEGER, INTENT(IN)::NDAYS,BEGIN
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL(4) ,DIMENSION(2:200,0:NDAYS,1:4),INTENT(OUT)::MA
    REAL(4) ,DIMENSION(4)::SMAM
    REAL(4)  KAPPA; INTEGER l,MU

    DO MU=2,200

    KAPPA=2.0/REAL(MU+1.0)

    ! Calculate the simple moving average, period tau.
    SMAM(1:4)=[ SUM(DPV(BEGIN:MU+BEGIN,2:5),DIM=1)/REAL(MU) ]

    MA(MU,0,1:4)=[ KAPPA*DPV(0,2:5)+SMAM(1:4)*(1.0-KAPPA) ]

    ! Calculate the exponential moving average, period tau.
    !---------v2.10---------------
    ! No need to loop over k, Array assign function [ ] solves this issue.
    DO l=BEGIN,NDAYS
    MA(MU,l,1:4)=[ KAPPA * DPV(l,2:5) + MA(MU,l-1,1:4) * (1.0-KAPPA) ]
    END DO
    !>>>>>>>>>>>
    !    DO l=1,N
    !    WRITE(*,'(4(F6.2),1X)') MA(TAU,l,1:4)
    !    END DO
    !>>>>>>>>>>
    ENDDO

                                                        END SUBROUTINE




!------------------------------------------------------------------------------------------------------------------------------------




                                SUBROUTINE GENERALSTATS(i,NDAYS,BEGIN,DPV,PBUYEXEC,PSELLEXEC,CO,ALLCOMPANIES)

    IMPLICIT NONE

    INTEGER, INTENT(IN)::i,NDAYS,BEGIN,ALLCOMPANIES
    CHARACTER(len=15),DIMENSION(ALLCOMPANIES),INTENT(IN)::CO
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL,DIMENSION(-20:20),INTENT(OUT)::PSELLEXEC,PBUYEXEC


    REAL,DIMENSION(1:NDAYS,-20:20)::SELLEXEC,BUYEXEC
    INTEGER l,k


    ! GENERATE DEFINITIONS
    SELLEXEC=0
    BUYEXEC=0


    DO l= BEGIN,NDAYS !SEGMENT(T-1),SEGMENT(T)  !START L DAY CYCLE


        DO k = -20,20
            !IF ( (1+k/real(5000))*DPV(l-1,4) <= DPV(l,2) ) THEN
            !IF ( (1+k/real(100))*SUM(DPV(l-1,2:5))/4.0 <= DPV(l,2) ) THEN
            IF ( (1 + k/real(1000) ) * DPV(l-1,4) < DPV(l,2) ) THEN
            SELLEXEC(l,k)=SELLEXEC(l,k)+1.0
            ELSE IF ( (1 + k/real(1000) )*DPV(l-1,4) > DPV(l,3) ) THEN
            BUYEXEC(l,k)=BUYEXEC(l,k)+1.0
            ENDIF
        ENDDO


    ENDDO ! END SEGMENTED L CYCLE


    PSELLEXEC(-20:20)=SUM(SELLEXEC(1:NDAYS,-20:20),DIM=1) / REAL(NDAYS-BEGIN)
    PBUYEXEC(-20:20)=SUM(BUYEXEC(1:NDAYS,-20:20),DIM=1) / REAL(NDAYS-BEGIN)

    !CONCLUSIONS:
    !1. STOCKS REMAIN ABOVE OR BELOW A GAP ONLY 30% OF THE TIME IN CANADA AND MEXICO AND THE US, THE OTHER 70% I GET FILLED AT THE PREVIOUS CLOSE.
    !2. LESS THAN 50% OF THE TIME I CAN GET FILLED BETTER THAN THE PREVIOUS CLOSE.

    WRITE(*,'(A10,40(F4.2,1X))') CO(i),PSELLEXEC(-20:20)
    WRITE(*,'(A10,40(F4.2,1X))') CO(i),PBUYEXEC(-20:20)


                                                        END SUBROUTINE
!------------------------------------------------------------------------------------------------------------------------------------

                                                          END MODULE


!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------
















!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE ATCXMETHODMOD


                                                           CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------

                SUBROUTINE ATCXMETHOD(BEGIN,NDAYS,DPV,CODATE,GP,PATTERNBACKDATE,MAXHOLDING,MAXHISTORY,HISTORYSTEP,MAXMEMORY)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER, INTENT(IN)::NDAYS,BEGIN,MAXHOLDING,MAXHISTORY,HISTORYSTEP,MAXMEMORY
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    INTEGER, DIMENSION(0:NDAYS),INTENT(IN)::CODATE

    REAL,DIMENSION(NDAYS),INTENT(OUT)::GP
    INTEGER,DIMENSION(MAXMEMORY,NDAYS,1:2),INTENT(OUT)::PATTERNBACKDATE

    !LOCAL_VAR_TMP
    INTEGER M,J,N,L,HOLDINGDAYS
    REAL LIMIT
!REAL(4) ,DIMENSION(0:NDAYS)::MIDDPV
    REAL(8),DIMENSION(MAXMEMORY,NDAYS)::AH,AL!,A

    REAL(8),DIMENSION(MAXMEMORY,MAXHISTORY*HISTORYSTEP,NDAYS)::BH,BL
    REAL,DIMENSION(MAXMEMORY,MAXHISTORY*HISTORYSTEP,NDAYS)::C

    REAL,DIMENSION(MAXMEMORY,NDAYS,MAXHOLDING)::PATTERN


    !INIT
    BH=0;BL=0;AH=0;AL=0
    LIMIT=0.015
    PATTERN=0
    GP=0
    C=0



!$OMP PARALLEL !NUM_THREADS(8)
    !$OMP DO

    DO M=10,MAXMEMORY,10 !PICTURE LENGTH

        DO L=BEGIN+MAXHISTORY*HISTORYSTEP+M,NDAYS

        AH(M,L-M:L) = DPV(L-M:L,2) / (SUM(DPV(L-M:L,2)**2) )**(0.5)

        AL(M,L-M:L) = DPV(L-M:L,3) / (SUM(DPV(L-M:L,3)**2) )**(0.5)

!        ENDDO
!    ENDDO

!!$OMP END PARALLEL DO

!!$OMP PARALLEL DO
!    DO M=20,MAXMEMORY,10 !SAME PICTURE LENGTH BUT GOING BACK IN TIME

        DO N=MAXHOLDING,MAXHISTORY*HISTORYSTEP

!            DO L=BEGIN+MAXHISTORY*HISTORYSTEP+M,NDAYS

            BH(M,N,L-M-N:L-N)=  DPV(L-M-N:L-N,2) / (SUM(DPV(L-M-N:L-N,2)**2) )**(0.5)

            BL(M,N,L-M-N:L-N)=  DPV(L-M-N:L-N,3) / (SUM(DPV(L-M-N:L-N,3)**2) )**(0.5)

!            ENDDO
!        ENDDO
!    ENDDO

!!$OMP END PARALLEL DO

!!$OMP PARALLEL DO

!    DO M=20,MAXMEMORY,10 !SAME PICTURE LENGTH BUT GOING BACK IN TIME

!        DO N=MAXHOLDING,MAXHISTORY*HISTORYSTEP

!            DO L=BEGIN+MAXHISTORY*HISTORYSTEP+M,NDAYS

                DO J=1,M
                IF (AH(M,L-J) < BH(M,N,L-N-J)+BH(M,N,L-N-J)*LIMIT .AND. AL(M,L-J) > BL(M,N,L-N-J)-BL(M,N,L-N-J)*LIMIT ) THEN
                !!$OMP ATOMIC
                C(M,N,L)=C(M,N,L)+1
                ENDIF
                ENDDO

!            ENDDO
!        ENDDO
!    ENDDO

!!$OMP END PARALLEL DO

!!$OMP PARALLEL DO

!    DO M=20,MAXMEMORY,10 !SAME PICTURE LENGTH BUT GOING BACK IN TIME

!        DO N=MAXHOLDING,MAXHISTORY*HISTORYSTEP

!            DO L=BEGIN+MAXHISTORY*HISTORYSTEP+M,NDAYS

            IF (C(M,N,L) > .85*M) THEN
                DO HOLDINGDAYS=1,MAXHOLDING
                PATTERN(M,L,HOLDINGDAYS)= ( DPV(L-N+HOLDINGDAYS,4) / DPV(L-N,4) ) - 1
                ENDDO

            PATTERNBACKDATE(M,l,1:2)=[CODATE(L-N-M),CODATE(L-N)]
            ENDIF

            ENDDO
        ENDDO
    ENDDO

!OMP END DO
!$OMP END PARALLEL


    DO L=BEGIN+MAXHISTORY*HISTORYSTEP+MAXMEMORY,NDAYS

    GP(L)=SUM(PATTERN(1:MAXMEMORY,L,1:MAXHOLDING))*100

    ENDDO










!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE

                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------










!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE TIDXMETHODMOD


                                                           CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------


    SUBROUTINE TIDXMETHOD(BEGIN,NDAYS,DPV,RESISTANCEMEAN,RESISTANCELEVEL,&
                SUPPORTMEAN,SUPPORTLEVEL,MAXPARTITION,MAXHISTORY,HISTORYSTEP)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::NDAYS,MAXPARTITION,MAXHISTORY,HISTORYSTEP,BEGIN
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL,DIMENSION(1:MAXHISTORY,1:MAXPARTITION,1:NDAYS),INTENT(OUT)::RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL

    !LOCAL_VAR_TMP
    INTEGER E,L,MAXPARTS,PARTITION,MAXTDAYS,j
    REAL(4),DIMENSION(1:MAXHISTORY,0:MAXPARTITION,MAXPARTITION+4,1:NDAYS):: MAXDPVSET,MINDPVSET
    INTEGER,DIMENSION(0:MAXPARTITION+4)::SEGMENT
    REAL(4),DIMENSION(MAXHISTORY,0:MAXPARTITION,NDAYS)::SUMYmax,SUMX,SUMX2,SUMY2max,SUMXYmax,SUMY2min,SUMXYmin,SUMYmin
    REAL(4) ,DIMENSION(MAXHISTORY,1:MAXPARTITION,NDAYS)::Mmax,Bmax,Mmin,Bmin
!    REAL(4) ,DIMENSION(1:MAXHISTORY,1:MAXPARTITION,NDAYS)::Rmin,Rmax

!----------------------------------------------------------------

    DO j=1,MAXHISTORY,1

!    MAXTDAYS=(HISTORYSTEP*4)*J
    MAXTDAYS=HISTORYSTEP*J
!----------------------------------------------------------------
    !INIT
    SUMX=0;SUMX2=0
    SUMXYmax=0;SUMYmax=0;SUMY2max=0
    SUMXYmin=0;SUMYmin=0;SUMY2min=0

!----------------------------------------------------------------

    DO PARTITION=1,MAXPARTITION
    MAXPARTS=PARTITION+4

    DO l=BEGIN+MAXTDAYS,NDAYS

    SUMXYmax(J,0,L)=0
    SUMXYmin(J,0,L)=0
    SEGMENT(0)= L - MAXTDAYS

!D INTERNAL VARIALBLE THAT MOVES THE MAX/MIN FINDER ACROSS PARTITIONS WITHIN THE MAXTDAYS RANGE
!        DO D=1,MAXPARTS

        SUMX(J,PARTITION,L)= MAXPARTS /REAL(2) * (MAXPARTS + 1.0)
        SUMX2(J,PARTITION,L)= ( MAXPARTS * (MAXPARTS+1.0) * (2.0 * MAXPARTS + 1.0) ) / REAL(6)


            DO  E=1,MAXPARTS

            SEGMENT(E)=  (E/REAL(MAXPARTS) -1.0 )*MAXTDAYS + l
            IF (SEGMENT(E) > NDAYS) SEGMENT(E)=NDAYS

            MAXDPVSET(J,PARTITION,E,L)= MAXVAL(DPV(SEGMENT(E-1)  : SEGMENT(E) ,2))
            MINDPVSET(J,PARTITION,E,L)= MINVAL(DPV(SEGMENT(E-1)  : SEGMENT(E) ,3))

            !WRITE(*,*) partition,e,MAXDPVSET(PARTITION,E,L),MINDPVSET(PARTITION,E,L)

            SUMXYmax(J,PARTITION,L)=( MAXDPVSET(J,PARTITION,E,L) * E ) + SUMXYmax(J,PARTITION,L)
            SUMXYmin(J,PARTITION,L)=( MINDPVSET(J,PARTITION,E,L) * E ) + SUMXYmin(J,PARTITION,L)

            !WRITE(*,*) SUMXYmax(J,PARTITION,L),SUMXYmin(J,PARTITION,L)

            ENDDO !END E CYCLE


        SUMYmax(J,PARTITION,L)= SUM ( MAXDPVSET(J,PARTITION,1:MAXPARTS,L))
        SUMY2max(J,PARTITION,L)=SUM ( (MAXDPVSET(J,PARTITION,1:MAXPARTS,L) )**2 )

        SUMYmin(J,PARTITION,L)= SUM ( MINDPVSET(J,PARTITION,1:MAXPARTS,L))
        SUMY2min(J,PARTITION,L)=SUM ( (MINDPVSET(J,PARTITION,1:MAXPARTS,L) )**2 )


!    WRITE(*,*) SUMYmax(J,PARTITION,L),SUMY2max(J,PARTITION,L),SUMYmin(J,PARTITION,L),SUMY2min(J,PARTITION,L)
!    WRITE(*,*) SUMX2(J,PARTITION,L),SUMX(J,PARTITION,L)

    ! COMPUTE SLOPE
    Mmax(J,PARTITION,L) = ( MAXPARTS * SUMXYmax(J,PARTITION,L)  -  &
    SUMX(J,PARTITION,L) * SUMYmax(J,PARTITION,L) ) / REAL( MAXPARTS * SUMX2(J,PARTITION,L) - (SUMX(J,PARTITION,L))**2 )

    ! COMPUTE Y-INTERCEPT
    Bmax(J,PARTITION,L) = (SUMYmax(J,PARTITION,L) *  SUMX2(J,PARTITION,L)  -  &
    SUMX(J,PARTITION,L) * SUMXYmax(J,PARTITION,L)) / REAL(MAXPARTS * SUMX2(J,PARTITION,L) - (SUMX(J,PARTITION,L))**2)

!ARITHMETIC ERROR (PENDING REVIEW)
    ! COMPUTE CORRELATION COEFFICIENT
!    Rmax(J,PARTITION,L) = (SUMXYmax(J,PARTITION,L) - SUMX(J,PARTITION,L) * SUMYmax(J,PARTITION,L) / REAL(MAXPARTS) ) / &
!    SQRT((SUMX2(J,PARTITION,L) - ( SUMX(J,PARTITION,L) )**2 / &
!    REAL( MAXPARTS * SUMY2max(J,PARTITION,L) - SUMYmax(J,PARTITION,L)**2/ REAL(MAXPARTS) )


    ! COMPUTE SLOPE
    Mmin(J,PARTITION,L) = ( MAXPARTS * SUMXYmin(J,PARTITION,L)  -  &
    SUMX(J,PARTITION,L) * SUMYmin(J,PARTITION,L) )/ REAL( MAXPARTS * SUMX2(J,PARTITION,L) - (SUMX(J,PARTITION,L))**2 )

    ! COMPUTE Y-INTERCEPT
    Bmin(J,PARTITION,L) = (SUMYmin(J,PARTITION,L) *  SUMX2(J,PARTITION,L)  -  &
    SUMX(J,PARTITION,L) * SUMXYmin(J,PARTITION,L)) / REAL(MAXPARTS * SUMX2(J,PARTITION,L) - (SUMX(J,PARTITION,L))**2)

!ARITHMETIC ERROR (PENDING REVIEW)
    ! COMPUTE CORRELATION COEFFICIENT
!    Rmin(J,PARTITION,L) = (SUMXYmin(J,PARTITION,L) - SUMX(J,PARTITION,L) * SUMYmin(J,PARTITION,L) / REAL(MAXPARTS) ) / &
!    SQRT((SUMX2(J,PARTITION,L) - ( SUMX(J,PARTITION,L) )**2 / &
!    REAL(MAXPARTS)) * (SUMY2min(J,PARTITION,L) - SUMYmin(J,PARTITION,L)**2/ REAL(MAXPARTS) ))


    !WRITE(*,*) Mmax(J,PARTITION,L),Mmin(J,PARTITION,L), Bmax(J,PARTITION,L),Bmin(J,PARTITION,L)

    RESISTANCELEVEL(J,PARTITION,L)=Mmax(J,PARTITION,L)*(MAXPARTS)+Bmax(J,PARTITION,L)
    RESISTANCEMEAN(J,PARTITION,L)=Mmax(J,PARTITION,L)
    SUPPORTLEVEL(J,PARTITION,L)=Mmin(J,PARTITION,L)*(MAXPARTS)+Bmin(J,PARTITION,L)
    SUPPORTMEAN(J,PARTITION,L)=Mmin(J,PARTITION,L)

    !WRITE(*,*) RESISTANCELEVEL(J,PARTITION,L),SUPPORTLEVEL(J,PARTITION,L),RESISTANCEMEAN(J,PARTITION,L),SUPPORTMEAN(J,PARTITION,L)

    ENDDO !END L CYCLE

    ENDDO ! END PARTITION CYLCE

    ENDDO

!!$OMP END DO
!!$OMP END PARALLEL

!    DO PARTITION=1,10,1
!    WRITE(*,*) PARTITION,RESISTANCELEVEL(PARTITION,NDAYS),SUPPORTLEVEL(PARTITION,NDAYS), &
!    RESISTANCEMEAN(PARTITION,NDAYS),SUPPORTMEAN(PARTITION,NDAYS)
!    ENDDO

!    ENDDO !END MAXTDAYS GENERATION

!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE

                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------











!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE STCXMETHODMOD


                                                            CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------


                SUBROUTINE STCXMETHOD(NDAYS,BEGIN,DPV,STOX,MAXSEG,MAXHISTORY,MAXSTOXAVG,MINSTOXAVG,HISTORYSTEP,MAXTAU,MINTAU)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::NDAYS,BEGIN,MAXSEG,MAXHISTORY,HISTORYSTEP,MAXTAU,MINTAU
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV

    REAL,DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(OUT)::MAXSTOXAVG,MINSTOXAVG
    REAL(4), DIMENSION(MAXTAU,NDAYS),INTENT(OUT)::STOX

    !LOCAL_VAR_TMP
    REAL(4),DIMENSION(MAXSEG+5,MAXSEG,MAXTAU,NDAYS,MAXHISTORY):: MAXSTOX,MINSTOX
    INTEGER,DIMENSION(0:MAXSEG+5)::SEGMENT
    INTEGER J,E,L,MAXPARTS,PARTITION,MAXTDAYS,TAU
    REAL(4), DIMENSION(MAXTAU,NDAYS)::D


!START

    DO TAU=MINTAU,MAXTAU
    DO L=BEGIN+TAU+1,NDAYS

    STOX(TAU,l) = (DPV(l,4) - MINVAL(DPV(L-TAU:l,3)) ) / (MAXVAL(DPV(L-TAU:l,2)) - MINVAL(DPV(L-TAU:l,3)) ) *100

    D(3,l)= SUM( STOX(TAU,l-3:l) ) / REAL(3+1)

    ENDDO
    ENDDO



!------------------------------------------------------------------------------------------------------------------------------------




    DO j=1,MAXHISTORY,1
    MAXTDAYS=HISTORYSTEP*J

    DO l=BEGIN+MAXTDAYS,NDAYS

    DO TAU=MINTAU,MAXTAU

    DO PARTITION=1,MAXSEG
    MAXPARTS=PARTITION+5


    DO  E=1,MAXPARTS

    SEGMENT(0)= L - MAXTDAYS

        SEGMENT(E)=  (E/REAL(MAXPARTS) - 1.0 )*MAXTDAYS + l
!        IF (SEGMENT(E) > NDAYS) SEGMENT(E)=NDAYS

        MAXSTOX(E,PARTITION,TAU,L,J)= MAXVAL(STOX(TAU, SEGMENT(E-1) : SEGMENT(E)))
        MINSTOX(E,PARTITION,TAU,L,J)= MINVAL(STOX(TAU, SEGMENT(E-1) : SEGMENT(E)))

    ENDDO !END E CYCLE

    MAXSTOXAVG(PARTITION,TAU,L,j)= SUM( MAXSTOX(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)
    MINSTOXAVG(PARTITION,TAU,L,j)= SUM( MINSTOX(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)

    ENDDO !END TAU CYCLE


    ENDDO !END l CYCLE
    ENDDO !END HISTORY CYCLE
    ENDDO !END PARTITION CYCLE


!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE

                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------











!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE OBVXMETHODMOD


                                                            CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------


            SUBROUTINE OBVXMETHOD(BEGIN,NDAYS,DPV,OBVI,MAXSEG,MAXHISTORY,MAXOBVIAVG,MINOBVIAVG,HISTORYSTEP,MINTAU,MAXTAU)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::NDAYS,BEGIN,MAXSEG,MAXHISTORY,HISTORYSTEP,MINTAU,MAXTAU
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS),INTENT(OUT)::OBVI
    REAL,DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(OUT)::MAXOBVIAVG,MINOBVIAVG

    !VAR_LOCAL_TMP
    REAL(4),DIMENSION(MAXSEG+5,MAXSEG,MAXTAU,NDAYS,MAXHISTORY):: MAXOBVI,MINOBVI
    INTEGER,DIMENSION(0:MAXSEG+5)::SEGMENT
    INTEGER J,E,L,MAXPARTS,PARTITION,MAXTDAYS,TAU,TAU0
    REAL(4) ,DIMENSION(1:NDAYS)::RETURNSOBV
    REAL(4) ,DIMENSION(0:NDAYS)::GAIN,LOSS,AVGAIN,AVLOSS
    REAL(4) ,DIMENSION(0:NDAYS)::OBV,OB

    !INIT
    GAIN=0;LOSS=0;AVGAIN=0;AVLOSS=0;MAXOBVI=0;MINOBVI=0;OBV=0;RETURNSOBV=0;OB=0


!    WRITE(*,*) 'OBVX STAGE 1'
    DO l=BEGIN+2,NDAYS

    IF (DPV(L-1,4) /= 0) THEN
    OBV(L)=OBV(L-1) + SIGN( DPV(l,6) , ( DPV(l,4) / DPV(l-1,4) ) - REAL(1) )
    ELSE
    OBV(L)=OBV(L-1)
    ENDIF

    IF (OBV(l-1) == 0) THEN
    RETURNSOBV(l) = 0
    ELSE
    RETURNSOBV(l)=(( OBV(l) / OBV(l-1) ) - 1 ) * 100.0
    ENDIF

!    WRITE(*,*) OBV(L),DPV(L,6),RETURNS(L,3),DPV(L,4),L

    ENDDO

!    WRITE(*,*) 'OBVX STAGE 2'
!---------------------------------------------------------------

    DO TAU=MINTAU,MAXTAU

    TAU0=TAU

    !----------------------------------
    !Average Initial OBVI calculation
    DO l=BEGIN,TAU0
            IF (RETURNSOBV(l)>0.0) THEN
            GAIN(l)=GAIN(l-1)+RETURNSOBV(l)
            ELSE IF (RETURNSOBV(l)<=0.0) THEN
            LOSS(l)=LOSS(l-1)+RETURNSOBV(l)
            END IF
    END DO

    !Set initial values for OBVIndex calculation

    AVGAIN(0)=GAIN(TAU0)/REAL(TAU0)
    AVLOSS(0)=LOSS(TAU0)/REAL(TAU0)

    !-------------------------------------------

    ! OBVI Calculation
    DO l=BEGIN,NDAYS

    IF (RETURNSOBV(l) > 0.0) THEN
    GAIN(l)=RETURNSOBV(l); LOSS(l)=0
    ELSE
    LOSS(l)=RETURNSOBV(l); GAIN(l)=0
    END IF

    AVGAIN(l)= ( AVGAIN(l-1)*REAL(TAU-1)+GAIN(l) )/REAL(TAU)

    AVLOSS(l)= ( AVLOSS(l-1)*REAL(TAU-1)+LOSS(l) )/REAL(TAU)

!    WRITE(*,*) L,AVGAIN(L),AVLOSS(L)

    IF ( AVLOSS(l) == 0 .OR. AVGAIN(l) == 0) THEN
    OB(l)= OB(l-1)
    ELSE
    OB(l)= ABS( AVGAIN(l) / AVLOSS(l) )
    ENDIF

    IF (OB(L) /= -1 ) THEN
    OBVI(TAU,l)= 100 - 100 / REAL( 1 + OB(l) )
    ELSE
    OBVI(TAU,l)= OBVI(TAU,l-1)
    ENDIF

!    WRITE(*,*) AVLOSS(l), AVGAIN(l),OBV(L),OBVI(TAU,l)

    ENDDO

    ENDDO

!    WRITE(*,*) 'OBVX STAGE 3'
!----------------------------------------------------------------

    DO j=1,MAXHISTORY,1
    MAXTDAYS=HISTORYSTEP*J

    DO l=BEGIN+MAXTDAYS,NDAYS

    DO TAU=MINTAU,MAXTAU

    DO PARTITION=1,MAXSEG
    MAXPARTS=PARTITION+5

    DO  E=1,MAXPARTS

    SEGMENT(0)= L - MAXTDAYS

        SEGMENT(E)=  (E/REAL(MAXPARTS) - 1.0 )*MAXTDAYS + l

        MAXOBVI(E,PARTITION,TAU,L,J)= MAXVAL(OBVI(TAU, SEGMENT(E-1) : SEGMENT(E)))
        MINOBVI(E,PARTITION,TAU,L,J)= MINVAL(OBVI(TAU, SEGMENT(E-1) : SEGMENT(E)))

    ENDDO !END E CYCLE

    MAXOBVIAVG(PARTITION,TAU,L,j)= SUM( MAXOBVI(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)
    MINOBVIAVG(PARTITION,TAU,L,j)= SUM( MINOBVI(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)

    ENDDO !END TAU CYCLE

    ENDDO !END l CYCLE
    ENDDO !END HISTORY CYCLE
    ENDDO !END PARTITION CYCLE


!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE

                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------










!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE RSIXMETHODMOD


                                                                CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------
                SUBROUTINE RSIXMETHOD(NDAYS,RSI,BEGIN,MAXSEG,MAXHISTORY,MAXRSIAVG,MINRSIAVG,HISTORYSTEP,DPV,MINTAU,MAXTAU)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::NDAYS,BEGIN,MAXSEG,MAXHISTORY,HISTORYSTEP,MINTAU,MAXTAU
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS,1:4),INTENT(OUT)::RSI
    REAL,DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(OUT)::MAXRSIAVG,MINRSIAVG

    !LOC_VAR_TEMP
    INTEGER l,k,TAU0,TAU,J,E,MAXPARTS,PARTITION,MAXTDAYS
    REAL(4) ,DIMENSION(0:NDAYS,1:4)::GAIN,LOSS
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS,1:4)::AVGAIN,AVLOSS,RS
    REAL(4),DIMENSION(MAXSEG+5,MAXSEG,MAXTAU,NDAYS,MAXHISTORY):: MAXRSI,MINRSI
    INTEGER,DIMENSION(0:MAXSEG+5)::SEGMENT
    REAL(4) ,DIMENSION(1:NDAYS,1:4)::RETURNS

    !INIT

    GAIN=0;LOSS=0;AVGAIN=0;AVLOSS=0;RS=0;MAXRSI=0;MINRSI=0;RETURNS=0

    DO l=BEGIN+2,NDAYS

        DO k=1,4
            IF ( DPV(l-1,k+1) /= 0) THEN
            RETURNS(l,k)=(( DPV(l,k+1) / DPV(l-1,k+1) ) - 1 ) * 100.0 !ALGORITHM IS FASTER FOR A DO K=1,4 ASSIGNATION LOOP
            ELSE
            RETURNS(l,k)=RETURNS(l-1,k)
            ENDIF
        ENDDO

    ENDDO


    !START
    DO TAU=MINTAU,MAXTAU

    TAU0=TAU

    !----------------------------------
    !Average Initial RSI calculation
        DO l=BEGIN,TAU0
            DO k=1,4
                IF (RETURNS(l,k) > 0) THEN
                GAIN(l,k)= GAIN(l-1,k)+RETURNS(l,k)
                ELSE
                LOSS(l,k)= LOSS(l-1,k)+RETURNS(l,k)
                ENDIF
            END DO
        END DO

        !Set initial values for RSIndex calculation
        DO k=1,4
        AVGAIN(TAU,TAU0,k)=GAIN(TAU0,k)/REAL(TAU0)
        AVLOSS(TAU,TAU0,k)=LOSS(TAU0,k)/REAL(TAU0)
        ENDDO

    !-------------------------------------------
        ! RSI Calculation
        DO l=BEGIN,NDAYS
            DO K=1,4
                IF ( RETURNS(l,k) > 0) THEN
                GAIN(l,k)=RETURNS(l,k);LOSS(l,k)=0
                ELSE
                LOSS(l,k)=RETURNS(l,k);GAIN(l,k)=0
                ENDIF
            END DO

            DO k=1,4
            AVGAIN(TAU,l,k)= ( AVGAIN(TAU,l-1,k)*REAL(TAU-1) + GAIN(l,k) ) /REAL(TAU)
            AVLOSS(TAU,l,k)= ( AVLOSS(TAU,l-1,k)*REAL(TAU-1) + LOSS(l,k) ) /REAL(TAU)

            IF ( AVGAIN(TAU,l,k) == 0 .OR. AVLOSS(TAU,l,k) == 0) THEN
            RS(TAU,l,k)= RS(TAU,l-1,k)
            ELSE
            RS(TAU,l,k)= ABS(AVGAIN(TAU,l,k) / AVLOSS(TAU,l,k))
            ENDIF

            IF (RS(TAU,l,k) /= -1 ) THEN
            RSI(TAU,l,k)= 100-100/REAL(1+RS(TAU,l,k))
            ELSE
            RSI(TAU,l,k)= RSI(TAU,l-1,k)
            ENDIF
            ENDDO

        ENDDO

    ENDDO

 !----------------------------------------------------------------


    DO j=1,MAXHISTORY,1
    MAXTDAYS=HISTORYSTEP*J

    DO l=BEGIN+MAXTDAYS,NDAYS

    DO TAU=MINTAU,MAXTAU

    DO PARTITION=1,MAXSEG
    MAXPARTS=PARTITION+5

    DO  E=1,MAXPARTS

    SEGMENT(0)= L - MAXTDAYS

        SEGMENT(E)=  (E/REAL(MAXPARTS) - 1.0 )*MAXTDAYS + l

        MAXRSI(E,PARTITION,TAU,L,J)= MAXVAL(RSI(TAU, SEGMENT(E-1) : SEGMENT(E) ,3))
        MINRSI(E,PARTITION,TAU,L,J)= MINVAL(RSI(TAU, SEGMENT(E-1) : SEGMENT(E) ,3))

    ENDDO !END E CYCLE

    MAXRSIAVG(PARTITION,TAU,L,j)= SUM( MAXRSI(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)
    MINRSIAVG(PARTITION,TAU,L,j)= SUM( MINRSI(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)

    ENDDO !END TAU CYCLE


    ENDDO !END l CYCLE

    ENDDO !END HISTORY CYCLE
    ENDDO !END PARTITION CYCLE


                                                        END SUBROUTINE


                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------





!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE MACXMETHODMOD

                                                            CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------
            SUBROUTINE MACXMETHOD(NDAYS,BEGIN,DPV,MAOZ,MAXSEG,MAXHISTORY,MAXMAOZAVG,MINMAOZAVG,HISTORYSTEP,MINTAU,MAXTAU)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER, INTENT(IN)::NDAYS,BEGIN,MAXSEG,MAXHISTORY,HISTORYSTEP,MINTAU,MAXTAU
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    REAL(4), DIMENSION(MAXTAU,NDAYS),INTENT(OUT)::MAOZ
    REAL,DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(OUT)::MAXMAOZAVG,MINMAOZAVG

    !LOCAL_VAR_TMP
    REAL KAPPAFAST,KAPPASLOW,KAPPASIGNAL,SMAM
    REAL(4), DIMENSION(MAXTAU,NDAYS)::MACD,MAFAST,MASLOW,SIGNAL
    REAL(4),DIMENSION(MAXSEG+5,MAXSEG,MAXTAU,NDAYS,MAXHISTORY):: MAXMAOZ,MINMAOZ
    INTEGER,DIMENSION(0:MAXSEG+5)::SEGMENT
    INTEGER J,E,L,MAXPARTS,PARTITION,MAXTDAYS,TAU

!------------------------------------------------------------------------------------------------------------------------------------

    WRITE(*,*) 'MACX STAGE 1'

    DO TAU=MINTAU,MAXTAU

    KAPPAFAST=2.0/REAL(TAU+1.0)
    KAPPASLOW=2.0/REAL(2*TAU+1.0)
    KAPPASIGNAL=2.0/REAL(TAU-3+1.0)

!    SMAMFAST(1:4)=[ SUM(DPV(BEGIN:TAU+BEGIN,2:5),DIM=1)/REAL(TAU) ]
!    MASFAST(TAU,0,1:4)=[ KAPPAFAST*DPV(0,2:5)+SMAMFAST(1:4)*(1.0-KAPPAFAST) ]

!    SMAMSLOW(1:4)=[ SUM(DPV(BEGIN:2*TAU+BEGIN,2:5),DIM=1)/REAL(2*TAU) ]
!    MASLOW(TAU,0,1:4)=[ KAPPASLOW*DPV(0,2:5)+SMAMSLOW(1:4)*(1.0-KAPPASLOW) ]

    SMAM= SUM(DPV(BEGIN:TAU+BEGIN,4)) / REAL(TAU)

    MAFAST(TAU,TAU+BEGIN+1)= SMAM*(1.0-KAPPAFAST) + KAPPAFAST * DPV(TAU+BEGIN+1,4)
    MASLOW(TAU,TAU+BEGIN+1)= SMAM*(1.0-KAPPASLOW) + KAPPASLOW * DPV(TAU+BEGIN+1,4)

    !TO FILL EMPTY SPACES
    MAFAST(TAU,1:TAU+BEGIN+1)= SMAM*(1.0-KAPPAFAST) + KAPPAFAST * DPV(TAU+BEGIN+1,4)
    MASLOW(TAU,1:TAU+BEGIN+1)= SMAM*(1.0-KAPPASLOW) + KAPPASLOW * DPV(TAU+BEGIN+1,4)

    DO L=BEGIN+TAU+2,NDAYS

!    MAFAST(TAU,l,1:4)=[ KAPPAFAST * DPV(l,2:5) + MAFAST(TAU,l-1,1:4) * (1.0-KAPPAFAST) ]
!    MASLOW(TAU,l,1:4)=[ KAPPASLOW * DPV(l,2:5) + MASLOW(TAU,l-1,1:4) * (1.0-KAPPASLOW) ]

    MAFAST(TAU,l)= (1 - KAPPAFAST) *  MAFAST(TAU,l-1)  + KAPPAFAST * DPV(l,4)
    MASLOW(TAU,l)= (1 - KAPPASLOW) *  MASLOW(TAU,l-1)  + KAPPASLOW * DPV(l,4)

    MACD(TAU,l)=MAFAST(TAU,l) - MASLOW(TAU,l)

    ENDDO

    DO L=BEGIN+TAU+3,NDAYS

    SIGNAL(TAU-3,l)= (1 - KAPPASIGNAL) * MACD(TAU,l-1) + KAPPASIGNAL * MACD(TAU,l)

    MAOZ(TAU,l)= MACD(TAU,l) - SIGNAL(TAU-3,l)

    ENDDO

    ENDDO


    WRITE(*,*) 'MACX STAGE 2'
!----------------------------------------------------------------


    DO j=1,MAXHISTORY,1
    MAXTDAYS=HISTORYSTEP*J

    DO l=BEGIN+MAXTDAYS,NDAYS

    DO TAU=MINTAU,MAXTAU

    DO PARTITION=1,MAXSEG
    MAXPARTS=PARTITION+5

    DO  E=1,MAXPARTS

    SEGMENT(0)= L - MAXTDAYS

        SEGMENT(E)=  (E/REAL(MAXPARTS) - 1.0 )*MAXTDAYS + l

        MAXMAOZ(E,PARTITION,TAU,L,J)= MAXVAL(MAOZ(TAU, SEGMENT(E-1) : SEGMENT(E)))
        MINMAOZ(E,PARTITION,TAU,L,J)= MINVAL(MAOZ(TAU, SEGMENT(E-1) : SEGMENT(E)))

    ENDDO !END E CYCLE

    MAXMAOZAVG(PARTITION,TAU,L,j)= SUM( MAXMAOZ(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)
    MINMAOZAVG(PARTITION,TAU,L,j)= SUM( MINMAOZ(1:MAXPARTS,PARTITION,TAU,L,j) ) / REAL(MAXPARTS)

    ENDDO !END TAU CYCLE


    ENDDO !END l CYCLE

!    WRITE(*,*) j,14,NDAYS,RSI(14,NDAYS,4),MAXPARTS,MAXRSIAVG(14,NDAYS,j,PARTITION)
!    WRITE(*,*) j,14,NDAYS,RSI(14,NDAYS,4),MAXPARTS,MINRSIAVG(14,NDAYS,j,PARTITION)

    ENDDO !END HISTORY CYCLE
    ENDDO !END PARTITION CYCLE



                                                            END SUBROUTINE

!------------------------------------------------------------------------------------------------------------------------------------

                                                              END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------












!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE GNUPLOTCODEMOD


                                                            CONTAINS


!------------------------------------------------------------------------------------------------------------------------------------

    SUBROUTINE GNUPLOTCODE(NDAYS,ORDER,TAU,HISTORY,SEG,HISTORYSTEP,MAXHISTORY,ALLCOMPANIES,CO,CODATE,MAXSEG,MAXPARTITION,&
                DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,I,MAXMEMORY,&
                MAXMAOZAVG,MINMAOZAVG,RESISTANCELEVEL,RESISTANCEMEAN,SUPPORTLEVEL,SUPPORTMEAN,ACTION,PRIME,MAXTAU,MAXORDER)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::I,NDAYS,ORDER,TAU,HISTORY,SEG,HISTORYSTEP,MAXHISTORY,ALLCOMPANIES,MAXSEG,MAXPARTITION,MAXTAU,&
                        MAXMEMORY,MAXORDER
    CHARACTER(len=15),DIMENSION(ALLCOMPANIES),INTENT(IN)::CO
    INTEGER, DIMENSION(0:NDAYS),INTENT(IN)::CODATE
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV,WDPV
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS,1:4),INTENT(IN)::RSI
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS),INTENT(IN)::OBVI,MAOZ
    REAL(4) ,DIMENSION(2:200,0:NDAYS,1:4),INTENT(IN)::MA
    REAL,DIMENSION(NDAYS),INTENT(IN)::GP
    INTEGER,DIMENSION(MAXMEMORY,NDAYS,1:2),INTENT(IN)::PATTERNBACKDATE
    REAL(4),DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(IN)::MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,&
    MAXMAOZAVG,MINMAOZAVG
    REAL,DIMENSION(MAXHISTORY,MAXPARTITION,NDAYS),INTENT(IN)::RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL
    CHARACTER(LEN=4),DIMENSION(0:MAXORDER,1:NDAYS),INTENT(IN)::ACTION
    INTEGER,DIMENSION(MAXORDER,1:7),INTENT(IN)::PRIME

    !LOCAL_VAR_TMP
    INTEGER MAXTDAYS,L
    INTEGER,DIMENSION(3)::CALENDARTODAY,CALENDARTODAY2
    CHARACTER(LEN=8)::CALENDARDAY

    INTEGER,PARAMETER::list2=20000
    INTEGER,PARAMETER::list3=40000


    MAXTDAYS=HISTORYSTEP*PRIME(ORDER,2)
!    DO l = BEGIN+MAXTDAYS,NDAYS

    DO l = NDAYS-250,NDAYS

    WRITE(CALENDARDAY,'(I8)') CODATE(l)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY(3),CALENDARTODAY(2),CALENDARTODAY(1)
    WRITE(i+list2,'(I0,A1,I0,A1,I0,1X, 26(F0.2,1X))') &
    CALENDARTODAY(3),'-',CALENDARTODAY(2),'-',CALENDARTODAY(1),&
    DPV(l,2:6),&
    MA(10,l,1:4),&
    MA(50,l,4),&
    MA(135,l,4),&
    WDPV(l,2:5),&
    OBVI(PRIME(ORDER,1),l),MINOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&

    MAOZ(PRIME(ORDER,1),l),MINMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&

    RSI(PRIME(ORDER,1),l,3),MINRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    GP(L)
    ENDDO


    WRITE(i+list3,*) 'set term png size 1726,1256'
    WRITE(i+list3,*) 'set output "',TRIM(CO(i)),'.png"'
    WRITE(i+list3,*) 'set xdata time'
    WRITE(i+list3,*) 'set timefmt "%Y-%m-%d"'
    WRITE(i+list3,*)'set datafile separator " "'
    WRITE(CALENDARDAY,'(I8)') CODATE(NDAYS-250)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY(3),CALENDARTODAY(2),CALENDARTODAY(1)

    WRITE(CALENDARDAY,'(I8)') CODATE(NDAYS)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY2(3),CALENDARTODAY2(2),CALENDARTODAY2(1)

    WRITE(i+list3,'(A13,I0,A1,I0,A1,I0,A3,I0,A1,I0,A1,I0,A2)') &
                    'set xrange ["',CALENDARTODAY(3),'-',CALENDARTODAY(2),'-',CALENDARTODAY(1),&
                     '":"',CALENDARTODAY2(3),'-',CALENDARTODAY2(2),'-',CALENDARTODAY2(1),'"]'

    WRITE(i+list3,*) 'show xrange'

    WRITE(i+list3,*) 'set grid'

    WRITE(i+list3,*) 'set multiplot layout 5,1'
    WRITE(i+list3,*) 'set tmargin 1'
    WRITE(i+list3,*) 'set bmargin 1'
    WRITE(i+list3,*) 'set lmargin 5'

    WRITE(i+list3,*) 'set format x ""'
    WRITE(i+list3,*) 'unset xlabel'

    WRITE(i+list3,*) 'set key inside bottom left'
    WRITE(i+list3,*) 'set style fill empty'
    WRITE(i+list3,*) 'plot "',TRIM(CO(i)),'" using 1:5:2:3:4 notitle with candlesticks lt 8, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:7 notitle with lines lt 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:8 notitle with lines lt 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:9 notitle with lines lt 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:10 notitle with lines lt 2, \'

    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:11 title "EMA(50)" with lines lt 3, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:12 title "EMA(135)" with lines lt 3, \'

    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:16:13:14:15 notitle with candlesticks lt 8'

    WRITE(i+list3,*) 'plot "',TRIM(CO(i)),'" using 1:17 title "OVI" with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:18 notitle with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:19 notitle with lines lw 2'

    WRITE(i+list3,*) 'plot "',TRIM(CO(i)),'" using 1:20 notitle with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:21 notitle with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:22 notitle with lines lw 2 '

    WRITE(i+list3,*) 'plot "',TRIM(CO(i)),'" using 1:23 notitle with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:24 notitle with lines lw 2, \'
    WRITE(i+list3,*) '"',TRIM(CO(i)),'" using 1:25 notitle with lines lw 2 '

    WRITE(i+list3,*) 'unset bmargin'
    WRITE(i+list3,*) 'unset format x'

    WRITE(i+list3,*) 'set xdata time'
    WRITE(i+list3,*) 'set timefmt "%Y-%m-%d"'
    WRITE(i+list3,*)'set datafile separator " "'
    WRITE(CALENDARDAY,'(I8)') CODATE(NDAYS-250)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY(3),CALENDARTODAY(2),CALENDARTODAY(1)
    WRITE(CALENDARDAY,'(I8)') CODATE(NDAYS)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY2(3),CALENDARTODAY2(2),CALENDARTODAY2(1)
    WRITE(i+list3,'(A16,I0,A1,I0,A1,I0,A3,I0,A1,I0,A1,I0,A2)') &
    'set xrange ["',CALENDARTODAY(3),'-',CALENDARTODAY(2),'-',CALENDARTODAY(1),&
    '":"',CALENDARTODAY2(3),'-',CALENDARTODAY2(2),'-',CALENDARTODAY2(1),'"]'

    WRITE(i+list3,*) 'plot "',TRIM(CO(i)),'" using 1:26 notitle with lines lw 2 '
    WRITE(i+list3,*) 'unset multiplot'
    WRITE(i+list3,*) 'unset output'



!------------------------------------------------------------------------------------------------------------------------------------
                                                        END SUBROUTINE
!------------------------------------------------------------------------------------------------------------------------------------

                                                        END MODULE


!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------










!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE PRINTCLOSEFORECASTMOD
    USE ZELLERMOD

                                                            CONTAINS


!------------------------------------------------------------------------------------------------------------------------------------

    SUBROUTINE PRINTCLOSEFORECAST(i,CO,MAXHOLDING,NDAYS,ORDER,HOLDINGDAYS,ACTION,CODATE,PRIME,ALLCOMPANIES,DPV,MAXORDER)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::MAXHOLDING,NDAYS,ORDER,HOLDINGDAYS,ALLCOMPANIES,i,MAXORDER
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV
    CHARACTER(len=15),DIMENSION(ALLCOMPANIES),INTENT(IN)::CO
    CHARACTER(LEN=4),DIMENSION(0:MAXORDER,1:NDAYS),INTENT(IN)::ACTION
    INTEGER, DIMENSION(0:NDAYS),INTENT(IN)::CODATE
    INTEGER,DIMENSION(MAXORDER,1:7),INTENT(IN)::PRIME


    !LOCAL_VAR_TMP
    CHARACTER(LEN=3)::DAYNAME
    INTEGER,DIMENSION(3)::CALENDARTODAY,CLOSEDATE
    INTEGER::MONTHTOTALDAYS=30
    CHARACTER(LEN=8)::CALENDARDAY
    INTEGER l
    CHARACTER(LEN=100),PARAMETER::FMT1='(I4,I2,I2,4X,I2,4X,A10,4X,A4,4X,A3,4X,I5,4X,A3)'
    CHARACTER(LEN=100),PARAMETER::FMT4='(A3,I4,I1,I1,I2,4X,I2,4X,A10,4X,A4,4X,A3,4X,I5,4X,A3)'
    CHARACTER(LEN=100),PARAMETER::FMT5='(A3,I4,I2,I1,I1,4X,I2,4X,A10,4X,A4,4X,A3,4X,I5,4X,A3)'
    CHARACTER(LEN=100),PARAMETER::FMT6='(A3,I4,I1,I1,I1,I1,4X,I2,4X,A10,4X,A4,4X,A3,4X,I5,4X,A3)'



    DO l=NDAYS-MAXHOLDING,NDAYS

    IF (  ACTION(ORDER,L) == "BUY" .OR. ACTION(ORDER,L) == "SELL" ) THEN

    IF (l+PRIME(ORDER,6) == NDAYS) THEN


    WRITE(CALENDARDAY,'(I8)') CODATE(l+PRIME(ORDER,6))
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY(3),CALENDARTODAY(2),CALENDARTODAY(1)

    !GENERATE CLOSE DATE
    IF (CALENDARTODAY(2)==04 .OR. CALENDARTODAY(2)==06 .OR. CALENDARTODAY(2)==09 .OR. CALENDARTODAY(2)==11) THEN
    MONTHTOTALDAYS=30
    ELSE IF (CALENDARTODAY(2) == 01 .OR. CALENDARTODAY(2) == 03 .OR. CALENDARTODAY(2) == 05 &
    .OR. CALENDARTODAY(2) == 07 .OR. CALENDARTODAY(2) == 08 .OR. CALENDARTODAY(2) == 10 &
    .OR. CALENDARTODAY(2) == 12) THEN
    MONTHTOTALDAYS=31
    ELSE IF (CALENDARTODAY(2)==02) THEN
    MONTHTOTALDAYS=28
    ENDIF

    CLOSEDATE(3)=CALENDARTODAY(3)
    CLOSEDATE(2)=CALENDARTODAY(2)
    CLOSEDATE(1)=CALENDARTODAY(1)

    IF (CALENDARTODAY(1)+PRIME(ORDER,6) > MONTHTOTALDAYS) THEN
    CLOSEDATE(3)=CALENDARTODAY(3)
    CLOSEDATE(2)=CALENDARTODAY(2)+1
    CLOSEDATE(1)=CALENDARTODAY(1)+PRIME(ORDER,6)-MONTHTOTALDAYS
    ENDIF

    CALL Zeller(CLOSEDATE,DAYNAME)

    IF (PRIME(ORDER,6) >= 4 .AND. (DAYNAME /= 'MON' .OR. DAYNAME/= 'SAT' .OR. DAYNAME/= 'SUN') ) THEN
    CLOSEDATE(1)=CALENDARTODAY(1)+PRIME(ORDER,6)+2
    ELSE
    CLOSEDATE(1)=CALENDARTODAY(1)+PRIME(ORDER,6)
    ENDIF

    CALL Zeller(CLOSEDATE,DAYNAME)

    IF (DAYNAME == 'SUN') THEN
    CLOSEDATE(1)=CLOSEDATE(1)+2
    ELSE IF (DAYNAME == 'SAT') THEN
    CLOSEDATE(1)=CLOSEDATE(1)+3
    ENDIF

    IF (CLOSEDATE(2) < 10 .AND. CLOSEDATE(1) > 10) THEN
    WRITE(27,FMT4) &
    DAYNAME,CLOSEDATE(3),0,CLOSEDATE(2),CLOSEDATE(1),0,TRIM(CO(i)),'SELL','DAY',NINT(1000/DPV(l,4)),'MOC'!,'MKT'
    ENDIF

    IF (CLOSEDATE(1) < 10 .AND. CLOSEDATE(2) > 10) THEN
    WRITE(27,FMT5) &
    DAYNAME,CLOSEDATE(3),CLOSEDATE(2),0,CLOSEDATE(1),0,TRIM(CO(i)),'SELL','DAY',NINT(1000/DPV(l,4)),'MOC'!,'MKT'
    ENDIF

    IF (CLOSEDATE(1) < 10 .AND. CLOSEDATE(2) < 10) THEN
    WRITE(27,FMT6) &
    DAYNAME,CLOSEDATE(3),0,CLOSEDATE(2),0,CLOSEDATE(1),0,TRIM(CO(i)),'SELL','DAY',NINT(1000/DPV(l,4)),'MOC'!,'MKT'
    ENDIF

    IF (CLOSEDATE(1) > 10 .AND. CLOSEDATE(2) > 10) THEN
    WRITE(27,FMT1) &
    DAYNAME,CLOSEDATE(3),CLOSEDATE(2),CLOSEDATE(1),0,TRIM(CO(i)),'SELL','DAY',NINT(1000/DPV(l,4)),'MOC'!,'MKT'
    ENDIF

    ENDIF

    ENDIF

    ENDDO
!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE
                                                        END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------








!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------


    MODULE PRINTOPENFORECASTMOD



                                                            CONTAINS


!------------------------------------------------------------------------------------------------------------------------------------

    SUBROUTINE PRINTOPENFORECAST(CO,MAXHISTORY,MAXSEG,MAXPARTITION,HISTORYSTEP,BEGIN,NDAYS,HOLDINGDAYS,MAXTAU,&
                ORDER,PARTITION,SEG,TAU,ACTION,CODATE,DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,COMPANY,VERSION,&
                MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,MAXMAOZAVG,MINMAOZAVG,PRIME,ALLCOMPANIES,HISTORY,MAXMEMORY,&
                RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTLEVEL,SUPPORTMEAN,CALENDARLOSS,CALENDARRETURNS,I,AGENT_ID,MAXORDER)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER,INTENT(IN)::HISTORYSTEP,BEGIN,NDAYS,HOLDINGDAYS,ORDER,PARTITION,SEG,TAU,&
                        MAXHISTORY,MAXPARTITION,MAXSEG,ALLCOMPANIES,HISTORY,I,MAXTAU,MAXMEMORY,MAXORDER
    CHARACTER(len=15),DIMENSION(ALLCOMPANIES),INTENT(IN)::CO
    CHARACTER(LEN=4),DIMENSION(0:MAXORDER,1:NDAYS),INTENT(IN)::ACTION
    CHARACTER(LEN=5),INTENT(IN):: VERSION
    INTEGER, DIMENSION(0:NDAYS),INTENT(IN)::CODATE
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV,WDPV
    REAL(4) ,DIMENSION(2:200,0:NDAYS,1:4),INTENT(IN)::MA
    REAL,DIMENSION(NDAYS),INTENT(IN)::GP
    INTEGER,DIMENSION(MAXMEMORY,NDAYS,1:2),INTENT(IN)::PATTERNBACKDATE
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS,1:4),INTENT(IN)::RSI
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS),INTENT(IN)::OBVI,MAOZ
    REAL(4),DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(IN)::MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,&
    MAXMAOZAVG,MINMAOZAVG
    REAL,DIMENSION(MAXHISTORY,MAXPARTITION,NDAYS),INTENT(IN)::RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL
    INTEGER,DIMENSION(MAXORDER,1:7),INTENT(IN)::PRIME
    CHARACTER(LEN=12),INTENT(IN)::COMPANY
    CHARACTER(LEN=2),INTENT(IN)::AGENT_ID

    !LOCAL_VAR_TMP
    INTEGER MAXTDAYS,L
    CHARACTER(LEN=8)::CALENDARDAY
    REAL,DIMENSION(NDAYS)::NETRETURN
    CHARACTER(LEN=3)::DAYNAME
    INTEGER,DIMENSION(3)::TODAY,CALENDARTODAY,NOW
    REAL,DIMENSION(1998:2020,12),INTENT(OUT)::CALENDARRETURNS
    REAL,DIMENSION(1998:2020,12),INTENT(OUT)::CALENDARLOSS
    CHARACTER(LEN=4),DIMENSION(1:12)::MONTH


    MONTH(1:12)= [ 'Jan ', 'Feb ','Mar ','Apr ','May ','June','July','Aug ','Sep ','Oct ','Nov ','Dec ' ]


    MAXTDAYS=HISTORYSTEP*PRIME(ORDER,2)
    DO l=BEGIN+MAXTDAYS,NDAYS

    IF (ACTION(ORDER,L) == "BUY" .OR. ACTION(ORDER,L) == "SELL") THEN

    IF (l+PRIME(ORDER,6) <= NDAYS) THEN

!    WRITE(*,*) 'WRITE TO PREDICTIONS'

    CALL iDATE(TODAY) ; WRITE(CALENDARDAY,'(I8)') CODATE(l)
    READ(CALENDARDAY,'(I4,I2,I2)') CALENDARTODAY(3),CALENDARTODAY(2),CALENDARTODAY(1)

    IF ( ACTION(ORDER,L) == "BUY" ) THEN
    NETRETURN(l)= DPV(l+PRIME(ORDER,6),4) / DPV(l+1,5) - 1
    ELSE IF ( ACTION(ORDER,L) == "SELL") THEN
    NETRETURN(l) = 1 - DPV(l+PRIME(ORDER,6),4) / DPV(l+1,5)
    ENDIF

    CALENDARRETURNS( CALENDARTODAY(3), CALENDARTODAY(2) ) = NETRETURN(l)*100 + &
    CALENDARRETURNS( CALENDARTODAY(3), CALENDARTODAY(2) )
    IF ( NETRETURN(l) < 0 ) THEN
    CALENDARLOSS( CALENDARTODAY(3), CALENDARTODAY(2) ) = NETRETURN(l)*100 + &
    CALENDARLOSS( CALENDARTODAY(3), CALENDARTODAY(2) )
    ENDIF


    IF ( CALENDARTODAY(3) == TODAY(3) .AND. CALENDARTODAY(2) == today(2) &
    .OR. ( CALENDARTODAY(1)+PRIME(ORDER,6) >= 30 .AND. CALENDARTODAY(2) == TODAY(2)-1  .AND.  CALENDARTODAY(3) == TODAY(3) ) ) THEN
    WRITE(100001,*) MONTH(CALENDARTODAY(2)),CALENDARTODAY(1),',',CALENDARTODAY(3), &
    '& ',ACTION(ORDER,l),'& ',COMPANY,'& ',DPV(l,4),'& ',&
    PRIME(ORDER,6),'& ',DPV(l+PRIME(ORDER,6),4),'& ',&
    (DPV(l+PRIME(ORDER,6),4)/DPV(l,5) - 1)*100,'\\'
    ENDIF

    ENDIF !END CALENDAR AND PAST PREDICTIONS FOR L+PRIME(ORDER,6) <= NDAYS

!------------------------------------------------------------

!    WRITE(*,*) 'WRITE TO AUTOMATION AND FORECAST SERVICE'

    IF (l == NDAYS) THEN

    !WRITE TO AUTOMATION
    WRITE(26,'(A2,1X,I8,1X,I2,1X,A10,1X,A4,1X,A3,1X,I3,1X,A3,F6.2)') &
    AGENT_ID,CODATE(l),PRIME(ORDER,6),TRIM(CO(i)),TRIM(ACTION(ORDER,l)),&
    'DAY',NINT(1000/DPV(l,4) ),&
    'LMT',DPV(l,4)

    !WRITE TO DEBUG
    WRITE(25,'(A5,1X,A2,1X,I8,1X,A4,1X,A7,1X,I2,1X, 2(F0.2,1X),9(F0.2,1X))') &
    VERSION,AGENT_ID,CODATE(l),TRIM(ACTION(ORDER,l)),TRIM(COMPANY),PRIME(ORDER,6),&
    SUPPORTLEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l),RESISTANCELEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l),&

    RSI(PRIME(ORDER,1),l,3), MINRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&

    OBVI(PRIME(ORDER,1),l), MINOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&

    MAOZ(PRIME(ORDER,1),l), MINMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)),&
    MAXMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2))

    ENDIF

    ENDIF
    ENDDO !END L DAYS CYCLE
!------------------------------------------------------------------------------------------------------------------------------------



!------------------------------------------------------------------------------------------------------------------------------------
                                                END SUBROUTINE
                                                END MODULE
!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------














!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    MODULE SLCTMETHODMOD

    USE SUBROUTINES
    USE GNUPLOTCODEMOD
    USE PRINTOPENFORECASTMOD
    USE PRINTCLOSEFORECASTMOD

    CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------


                SUBROUTINE SLCTMETHOD(NDAYS,RSI,DPV,ALLCOMPANIES,CO,COMPANY,I,MAXMEMORY,MAXORDER,&
                            MAXRSIAVG,MINRSIAVG,BEGIN,MA,RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL,&
                            STOCKEXCHANGE,CODATE,MAXPARTITION,MAXHISTORY,GP,HISTORYSTEP,PATTERNBACKDATE,PRIME,VERSION, &
                            MAXOBVIAVG,MINOBVIAVG,OBVI,MAXSEG,MAXHOLDING,MAOZ,MAXMAOZAVG,MINMAOZAVG,WDPV,MAXSEARCH,&
                            MINDELTA,MAXDELTA,MINSTAT,&
                            S1,S2,S3,S4,S1_AVG,S2_AVG,S3_AVG,S4_AVG,GRATIO_AVG,NETGAIN_AVG,NETLOSS_AVG,MAXEPSILON,&
                            CALENDARRETURNS,CALENDARLOSS,MAXTAU,MINTAU,GRATIO,NETGAIN,NETLOSS,AGENT_ID,MINEPSILON,&
                            NETGAINA1,NETLOSSA1,GAINERSA1,LOSERSA1,NETGAINA3,NETLOSSA3,GAINERSA3,LOSERSA3,&
                            PATHCOSTA1,PATHCOSTA3,PATHPROFITA1,PATHPROFITA3,PATHPROFITCOUNTA1,PATHPROFITCOUNTA3, &
                            PATHCOSTCOUNTA1,PATHCOSTCOUNTA3,MASTERA1,MASTERA3)

    IMPLICIT NONE

    !INTERFACE_VAR
    INTEGER, INTENT(IN)::I,ALLCOMPANIES,NDAYS,BEGIN,MAXPARTITION,MAXHISTORY,HISTORYSTEP,MAXSEG,MAXHOLDING,MAXSEARCH,MAXEPSILON,&
                        MAXTAU,MINTAU,MINEPSILON,MAXMEMORY,MAXORDER,MINDELTA,MAXDELTA
    REAL,INTENT(IN)::MINSTAT
    CHARACTER(LEN=3),INTENT(IN)::STOCKEXCHANGE
    CHARACTER(LEN=5),INTENT(IN)::VERSION
    INTEGER, DIMENSION(0:NDAYS),INTENT(IN)::CODATE
    REAL(4) ,DIMENSION(0:NDAYS,1:6),INTENT(IN)::DPV,WDPV
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS,1:4),INTENT(IN)::RSI
    REAL(4) ,DIMENSION(MAXTAU,0:NDAYS),INTENT(IN)::OBVI,MAOZ
    REAL(4) ,DIMENSION(2:200,0:NDAYS,1:4),INTENT(IN)::MA
    REAL,DIMENSION(NDAYS),INTENT(IN)::GP
    INTEGER,DIMENSION(MAXMEMORY,NDAYS,1:2),INTENT(IN)::PATTERNBACKDATE
    REAL(4),DIMENSION(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),INTENT(IN)::MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,&
                                                                 MAXMAOZAVG,MINMAOZAVG
    REAL,DIMENSION(1:MAXHISTORY,1:MAXPARTITION,1:NDAYS),INTENT(IN)::RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL
    CHARACTER(len=15),DIMENSION(ALLCOMPANIES),INTENT(IN)::CO
    CHARACTER(LEN=12),INTENT(IN)::COMPANY

    REAL,DIMENSION(0:7),INTENT(INOUT)::S1,S2,S3,S4
    REAL,DIMENSION(4),INTENT(INOUT)::GRATIO_AVG,NETGAIN_AVG,NETLOSS_AVG,GRATIO,NETGAIN,NETLOSS
    REAL,DIMENSION(1998:2020,12),INTENT(INOUT)::CALENDARRETURNS
    REAL,DIMENSION(1998:2020,12),INTENT(INOUT)::CALENDARLOSS
    REAL,DIMENSION(1:7),INTENT(INOUT)::S1_AVG,S2_AVG,S3_AVG,S4_AVG

    INTEGER,DIMENSION(MAXORDER,1:7),INTENT(INOUT)::PRIME
    CHARACTER(LEN=2),INTENT(OUT)::AGENT_ID

    REAL(4),DIMENSION(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    INTENT(OUT)::&
    NETGAINA1,NETLOSSA1,GAINERSA1,LOSERSA1,NETGAINA3,NETLOSSA3,GAINERSA3,LOSERSA3,&
    PATHCOSTA1,PATHCOSTA3,PATHPROFITA1,PATHPROFITA3,PATHPROFITCOUNTA1,PATHPROFITCOUNTA3,&
    PATHCOSTCOUNTA1,PATHCOSTCOUNTA3,MASTERA1,MASTERA3

    !LOCAL_VAR_TMP
    INTEGER l,SEARCH,HOLDINGDAYS,TAU,EPSILON,ORDER,PARTITION,HISTORY,MAXTDAYS,SEG,HOLDING,K,M,DELTA
    CHARACTER(LEN=4),DIMENSION(0:MAXORDER,1:NDAYS)::ACTION
    CHARACTER(LEN=4),DIMENSION(1:12)::MONTH
    REAL(4),DIMENSION(1:NDAYS)::DAILYRETURN,NETRETURN
    REAL MINPROFIT,MINLOSS,STOPLOSS,RATE

!--------------------------------------------------------------------------------------------------------------------------
    !INIT
    PRIME=0
    GAINERSA1=0; LOSERSA1=0; NETGAINA1=0 ; NETLOSSA1=0
    GAINERSA3=0; LOSERSA3=0; NETGAINA3=0 ; NETLOSSA3=0
    PATHCOSTA1=0;PATHCOSTA3=0; PATHPROFITA1=0;PATHPROFITA3=0
    PATHCOSTCOUNTA1=0;PATHCOSTCOUNTA3=0
    PATHPROFITCOUNTA1=0;PATHPROFITCOUNTA3=0
    MASTERA1=0; MASTERA3=0

    MONTH(1:12)= [ 'Jan ', 'Feb ','Mar ','Apr ','May ','June','July','Aug ','Sep ','Oct ','Nov ','Dec ' ]

    MINPROFIT=0!0.01
    MINLOSS=0!0.01

    STOPLOSS=3
    RATE=STOPLOSS/REAL(MAXHOLDING)
!--------------------------------------------------------------------------------------------------------------------------
    !$OMP PARALLEL DO

    DO HISTORY=1,MAXHISTORY
    MAXTDAYS=HISTORYSTEP*HISTORY
!-------------------------------------------------------------------------------------------------------------------------
    DO l = BEGIN+MAXTDAYS+MAXHISTORY*HISTORYSTEP,NDAYS !START L DAY CYCLE
    IF (GP(L) > 0 .OR. GP(L) < -0) THEN
!-------------------------------------------------------------------------------------------------------------------------
    DO TAU=MINTAU,MAXTAU

    DO SEG=1,MAXSEG
!-------------------------------------------------------------------------------------------------------------------------


!------------------------------------------------------!AGENT 1 STRATEGY 1-----------------------------------------------------------
                                        !COUNTER TREND BREAKING SUPPORT GOING TO RESISTANCE

    IF (  RSI(TAU,l,3) < MINRSIAVG(SEG,TAU,l,HISTORY) .AND. GP(L)>0 &
    .AND. OBVI(TAU,l) < MINOBVIAVG(SEG,TAU,l,HISTORY) .AND. MAOZ(TAU,l) < MINMAOZAVG(SEG,TAU,l,HISTORY) &
    ) THEN

    DO PARTITION=1,MAXPARTITION
    DO SEARCH=1,MAXSEARCH ; IF (L+SEARCH<=NDAYS) THEN
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO DELTA=MINDELTA,MAXDELTA

    IF ( &
    DPV(l+SEARCH,4)/DPV(l,4) < DELTA*0.5  &
    .AND. (DPV(l+SEARCH,4) - SUPPORTLEVEL(HISTORY,PARTITION,l+SEARCH))/&
    SUPPORTLEVEL(HISTORY,PARTITION,l+SEARCH) < -0.004*EPSILON &
    .AND. GP(L+SEARCH)>0 &
    ) THEN

!------------------------------------------------------------------------------------------------------------------------------------

        DO HOLDINGDAYS=1,MAXHOLDING

        IF (l+SEARCH+HOLDINGDAYS <= NDAYS) THEN

            NETRETURN(l+SEARCH+HOLDINGDAYS)= &
            DPV(l+SEARCH+HOLDINGDAYS,4) / DPV(l+SEARCH+1,5) - 1

            IF ( NETRETURN(l+SEARCH+HOLDINGDAYS)  > MINPROFIT ) THEN
            NETGAINA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            NETRETURN(l+SEARCH+HOLDINGDAYS)*100
            !!$OMP ATOMIC
            GAINERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            GAINERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ELSE
            NETLOSSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            NETRETURN(l+SEARCH+HOLDINGDAYS)*100
            !!$OMP ATOMIC
            LOSERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            LOSERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ENDIF


            DO HOLDING=1,HOLDINGDAYS

            DAILYRETURN(l+SEARCH+HOLDING)= &
            DPV(l+SEARCH+HOLDING,4) / DPV(l+SEARCH+HOLDING-1,4) - 1

            IF (DAILYRETURN(l+SEARCH+HOLDING) > 0 ) THEN
            !!$OMP ATOMIC
            PATHPROFITA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHPROFITA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
            DAILYRETURN(l+SEARCH+HOLDING)*100
            !!$OMP ATOMIC
            PATHPROFITCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHPROFITCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ELSE
            !!$OMP ATOMIC
            PATHCOSTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHCOSTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
            DAILYRETURN(l+SEARCH+HOLDING)*100
            !!$OMP ATOMIC
            PATHCOSTCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHCOSTCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ENDIF

            ENDDO



        ENDIF
        ENDDO !END HOLDING DAYS CYCLE



!----------------------------------------------------------------------------------------------------
    ENDIF

    ENDDO; ENDDO; ENDIF; ENDDO; ENDDO

!----------------------------------------------------------------------------------------------------
    ENDIF
!----------------------------------------------------------------------------------------------------


!----------------------------------------------!AGENT 3 STRATEGY 1-----------------------------------
                        !COUNTER TREND BREAKING RESISTANCE AND FALLING BACK TOWARDS SUPPORT


    IF ( RSI(TAU,l,3) > MAXRSIAVG(SEG,TAU,l,HISTORY) .AND. GP(L)<-0 &
    .AND. OBVI(TAU,l) > MAXOBVIAVG(SEG,TAU,l,HISTORY) .AND. MAOZ(TAU,l) > MAXMAOZAVG(SEG,TAU,l,HISTORY) &
    .AND. STOCKEXCHANGE /= 'SSX' .AND. STOCKEXCHANGE /= 'SZX') THEN

    DO PARTITION=1,MAXPARTITION
    DO SEARCH=1,MAXSEARCH ; IF (L+SEARCH<=NDAYS) THEN
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO DELTA=MINDELTA,MAXDELTA

    IF ( &
    DPV(l+SEARCH,4)/DPV(l,4) > DELTA*0.5  &
    .AND. (DPV(l+SEARCH,4) - RESISTANCELEVEL(HISTORY,PARTITION,l+SEARCH))/&
    RESISTANCELEVEL(HISTORY,PARTITION,l+SEARCH) > 0.004*EPSILON &
    .AND. GP(L+SEARCH)<-0 &
    ) THEN

!----------------------------------------------------------------------------------------------------

        DO HOLDINGDAYS=1,MAXHOLDING

        IF (l+SEARCH+HOLDINGDAYS <= NDAYS) THEN

            NETRETURN(l+SEARCH+HOLDINGDAYS)= &
            1 - DPV(l+SEARCH+HOLDINGDAYS,4) / DPV(l+SEARCH+1,5)

            IF ( NETRETURN(l+SEARCH+HOLDINGDAYS)  > MINPROFIT ) THEN
            NETGAINA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            NETRETURN(l+SEARCH+HOLDINGDAYS)*100
            !!$OMP ATOMIC
            GAINERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            GAINERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ELSE
            NETLOSSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            NETRETURN(l+SEARCH+HOLDINGDAYS)*100
            !!$OMP ATOMIC
            LOSERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            LOSERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.

            ENDIF

            DO HOLDING=1,HOLDINGDAYS

            DAILYRETURN(l+SEARCH+HOLDING)= &
            1 - DPV(l+SEARCH+HOLDING,4) / DPV(l+SEARCH+HOLDING-1,4)

            IF (DAILYRETURN(l+SEARCH+HOLDING) > 0 ) THEN
            !!$OMP ATOMIC
            PATHPROFITA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHPROFITA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
            DAILYRETURN(l+SEARCH+HOLDING)*100
            !$OMP ATOMIC
            PATHPROFITCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHPROFITCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ELSE
            !!$OMP ATOMIC
            PATHCOSTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHCOSTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
            DAILYRETURN(l+SEARCH+HOLDING)*100
            !!$OMP ATOMIC
            PATHCOSTCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=&
            PATHCOSTCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)+1.
            ENDIF

            ENDDO


        ENDIF; ENDDO



!----------------------------------------------------------------------------------------------------
    ENDIF

    ENDDO;ENDDO; ENDIF; ENDDO; ENDDO

!----------------------------------------------------------------------------------------------------
    ENDIF
!----------------------------------------------------------------------------------------------------





!------------------------------------------------------------------------------------------------------------------------------------
    ENDDO ! END TAU CYCLE
!------------------------------------------------------------------------------------------------------------------------------------
    ENDDO !END SEG CYCLE
!------------------------------------------------------------------------------------------------------------------------------------
    ENDIF !END GP PRESELECTION CRITERIA
    ENDDO !END L DAYS CYCLE
!------------------------------------------------------------------------------------------------------------------------------------
    ENDDO ! END HISTORY CYCLE
!------------------------------------------------------------------------------------------------------------------------------------

    !$OMP END PARALLEL DO




















!------------------------------------------------------------------------------------------------------------------------------------

!$OMP PARALLEL DO

    DO DELTA=MINDELTA,MAXDELTA
    DO HOLDINGDAYS=1,MAXHOLDING
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO PARTITION=1,MAXPARTITION
    DO SEG=1,MAXSEG
    DO HISTORY=1,MAXHISTORY
    DO TAU=MINTAU,MAXTAU


    IF (PATHCOSTCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > 0 .AND. &
    LOSERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > 0 ) THEN


    IF ( &

!    .AND. &
    (PATHPROFITCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)/ &
    ( PATHPROFITCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHCOSTCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) ) ) > &
    MINSTAT &

    .AND. &
    PATHCOSTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) < &
    PATHPROFITA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)*&
    (1-MINSTAT) &

    .AND. &
    NETLOSSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) < &
    NETGAINA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)*&
    (1-MINSTAT) &

    .AND. &
    (GAINERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) / &
    ( GAINERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    LOSERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) ) ) > &
    MINSTAT &

    .AND. &
    NETLOSSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > -STOPLOSS &

    ) THEN

    MASTERA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)= &
    NETGAINA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    NETLOSSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHPROFITCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) - &
    PATHCOSTCOUNTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHPROFITA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHCOSTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    GAINERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) - &
    LOSERSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)

    ENDIF
    ENDIF
    ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO


!$OMP END PARALLEL DO
!------------------------------------------------------------------------------------------------------------------------------------

    DO ORDER=1,MAXORDER

    IF (MAXVAL(MASTERA1) > 0) THEN

    PRIME(ORDER,1:7)= MAXLOC(MASTERA1)
    PRIME(ORDER,1)=LBOUND(MASTERA1,DIM=1) - 1 + PRIME(ORDER,1)
    PRIME(ORDER,5)=LBOUND(MASTERA1,DIM=5) - 1 + PRIME(ORDER,5)
    PRIME(ORDER,7)=LBOUND(MASTERA1,DIM=7) - 1 + PRIME(ORDER,7)

    ACTION=''
    AGENT_ID='A1'

!------------------------------------------------------------------------------------------------------------------------------------

    MAXTDAYS=HISTORYSTEP*PRIME(ORDER,2)

    DO l = BEGIN+MAXTDAYS,NDAYS

!------------------------------------------------------!AGENT 1 STRATEGY 1-----------------------------------------------------------
                                        !COUNTER TREND BREAKING SUPPORT GOING TO RESISTANCE

    IF (  RSI(PRIME(ORDER,1),l,3) < MINRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) .AND. GP(L)>0 &
    .AND. OBVI(PRIME(ORDER,1),l) < MINOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) .AND. &
    MAOZ(PRIME(ORDER,1),l) < MINMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) &
    ) THEN

    DO SEARCH=1,MAXSEARCH ; IF (L+SEARCH<=NDAYS) THEN

    IF ( &
    DPV(l+SEARCH,4)/DPV(l,4) < PRIME(ORDER,7)*0.5  &
    .AND. (DPV(l+SEARCH,4) - SUPPORTLEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l+SEARCH))/&
    SUPPORTLEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l+SEARCH) < -0.004*PRIME(ORDER,5) &
    .AND. GP(L+SEARCH)>0 &
    ) THEN

!------------------------------------------------------------------------------------------------------------------------------------

    ACTION(ORDER,l+SEARCH)='BUY'

!----------------------------------------------------------------------------------------------------
    ENDIF; ENDIF; ENDDO; ENDIF; ENDDO
!----------------------------------------------------------------------------------------------------
!    WRITE(*,*) 'CRITERIA EXEC SUCCESSFUL'

!    WRITE(*,*) 'RUNNING NO REPEAT'
    MAXTDAYS=PRIME(ORDER,2)*HISTORYSTEP

    IF (ORDER >= 2) THEN
    DO M=1,ORDER-1
        DO L=BEGIN+MAXTDAYS,NDAYS
            IF ( ACTION(ORDER,l) == ACTION(ORDER-M,l) ) THEN
            ACTION(ORDER,l)=''
            ENDIF
        ENDDO
    ENDDO
    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------
!    WRITE(*,*) 'RUNNING PRINT FORECAST'

    CALL PRINTOPENFORECAST(CO,MAXHISTORY,MAXSEG,MAXPARTITION,HISTORYSTEP,BEGIN,NDAYS,HOLDINGDAYS,MAXTAU,&
    ORDER,PARTITION,SEG,TAU,ACTION,CODATE,DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,COMPANY,VERSION,&
    MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,MAXMAOZAVG,MINMAOZAVG,PRIME,ALLCOMPANIES,HISTORY,MAXMEMORY,&
    RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTLEVEL,SUPPORTMEAN,CALENDARLOSS,CALENDARRETURNS,I,AGENT_ID,MAXORDER)

!    CALL PRINTCLOSEFORECAST(i,CO,MAXHOLDING,NDAYS,ORDER,HOLDINGDAYS,ACTION,CODATE,PRIME,ALLCOMPANIES,DPV,MAXORDER)

!    WRITE(*,*) 'RUNNING GNUPLOT CODE'

!    IF (ORDER==1) THEN
    CALL GNUPLOTCODE(NDAYS,ORDER,TAU,HISTORY,SEG,HISTORYSTEP,MAXHISTORY,ALLCOMPANIES,CO,CODATE,MAXSEG,MAXPARTITION,&
    DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,I,MAXMEMORY,&
    MAXMAOZAVG,MINMAOZAVG,RESISTANCELEVEL,RESISTANCEMEAN,SUPPORTLEVEL,SUPPORTMEAN,ACTION,PRIME,MAXTAU,MAXORDER)
!    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------
!    WRITE(*,*) 'RUNNING NUMBERS'

    WRITE(*,'(A7,1X,1X,I2,1X,A6,4X, 7(A4,1X,I2,1X))') &
    'AGENT 1', ORDER, 'PRIMES',&
    'TAU=',PRIME(ORDER,1),&
    'HIS=',PRIME(ORDER,2),&
    'SEG=',PRIME(ORDER,3),&
    'PAR=',PRIME(ORDER,4),&
    'EPS=',PRIME(ORDER,5),&
    'HOL=',PRIME(ORDER,6),&
    'DEL=',PRIME(ORDER,7)

    WRITE(*,'( F6.2,1X, I2,1X, F7.2,1X, I4,1X, 2(F10.2,1X), 2(F5.0,1X))') &
NETLOSSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
NINT(LOSERSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))),&
NETGAINA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
NINT(GAINERSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))),&
PATHCOSTA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHPROFITA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHCOSTCOUNTA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHPROFITCOUNTA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))


!    WRITE(*,*) ' RUNNING AVERAGES'
    !AVERAGES
    S1(0)=S1(0)+1

    S1(1)=S1(1)+PRIME(ORDER,1) ;S1(2)=S1(2)+PRIME(ORDER,2) ;S1(3)=S1(3)+PRIME(ORDER,3); S1(4)=S1(4)+PRIME(ORDER,4)
    S1(5)=S1(5)+PRIME(ORDER,5); S1(6)=S1(6)+PRIME(ORDER,6) ;S1(7)=S1(7)+PRIME(ORDER,7)

    S1_AVG(1)= S1(1) / REAL( S1(0) ); S1_AVG(2)= S1(2) / REAL( S1(0) ); S1_AVG(3)= S1(3) / REAL( S1(0) )
    S1_AVG(4)= S1(4) / REAL( S1(0) ); S1_AVG(5)= S1(5) / REAL( S1(0) ); S1_AVG(6)= S1(6) / REAL( S1(0) )
    S1_AVG(7)= S1(7) / REAL( S1(0) )

    GRATIO(1)= GRATIO(1) + &
    GAINERSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))&
    / ( GAINERSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))&
    + LOSERSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)))

    GRATIO_AVG(1)=GRATIO(1) / S1(0)

    NETGAIN(1)=NETGAIN(1)+&
    NETGAINA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))
    NETGAIN_AVG(1)=NETGAIN(1) / S1(0)

    NETLOSS(1)=NETLOSS(1)+&
    NETLOSSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))
    NETLOSS_AVG(1)= NETLOSS(1) / S1(0)

!------------------------------------------------------------------------------------------------------------------------------------
!    WRITE(*,*) ' RUNNING GRATIO CLEAN'

    MASTERA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))=0


    DO HOLDINGDAYS=1,MAXHOLDING
    DO DELTA=MINDELTA,MAXDELTA
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO PARTITION=1,MAXPARTITION
    DO SEG=1,MAXSEG
    DO HISTORY=1,MAXHISTORY
    DO TAU=MINTAU,MAXTAU

    IF (NETGAINA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        NETGAINA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        NETLOSSA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        NETLOSSA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        PATHCOSTA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        PATHCOSTA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        PATHPROFITA1(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        PATHPROFITA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) ) &

        MASTERA1(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=0

    ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO

!------------------------------------------------------------------------------------------------------------------------------------
    ENDIF 
    ENDDO !END ORDER CYCLE
!------------------------------------------------------------------------------------------------------------------------------------






















!------------------------------------------------------------------------------------------------------------------------------------

!!$OMP  PARALLEL DO

    DO DELTA=MINDELTA,MAXDELTA
    DO HOLDINGDAYS=1,MAXHOLDING
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO PARTITION=1,MAXPARTITION
    DO SEG=1,MAXSEG
    DO HISTORY=1,MAXHISTORY
    DO TAU=MINTAU,MAXTAU


    IF ( PATHCOSTCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > 0 .AND. &
    LOSERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > 0 ) THEN

    IF ( &

!    .AND. &
    (PATHPROFITCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)/ &
    ( PATHPROFITCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHCOSTCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) ) ) > &
    MINSTAT &

    .AND. &
    PATHCOSTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) < &
    PATHPROFITA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)*&
    (1-MINSTAT) &

    .AND. &
    NETLOSSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) < &
    NETGAINA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)*&
    (1-MINSTAT) &

    .AND. &
    (GAINERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) / &
    ( GAINERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    LOSERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) ) ) > &
    MINSTAT &

    .AND. &
    NETLOSSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) > -STOPLOSS &

    ) THEN

    MASTERA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)= &
    NETGAINA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    NETLOSSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHPROFITCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) - &
    PATHCOSTCOUNTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHPROFITA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    PATHCOSTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) + &
    GAINERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) - &
    LOSERSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)


    ENDIF
    ENDIF
    ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO


!!$OMP END PARALLEL DO
!------------------------------------------------------------------------------------------------------------------------------------

    DO ORDER=1,MAXORDER

    IF (MAXVAL(MASTERA3) > 0) THEN

    PRIME(ORDER,1:7)= MAXLOC(MASTERA3)
    PRIME(ORDER,1)=LBOUND(MASTERA3,DIM=1) - 1 + PRIME(ORDER,1)
    PRIME(ORDER,5)=LBOUND(MASTERA3,DIM=5) - 1 + PRIME(ORDER,5)
    PRIME(ORDER,7)=LBOUND(MASTERA3,DIM=7) - 1 + PRIME(ORDER,7)

    ACTION=''
    AGENT_ID='A3'


!------------------------------------------------------------------------------------------------------------------------------------

    MAXTDAYS=HISTORYSTEP*PRIME(ORDER,2)

    DO l = BEGIN+MAXTDAYS,NDAYS

!----------------------------------------------!AGENT 3 STRATEGY 1-----------------------------------
                        !COUNTER TREND BREAKING RESISTANCE AND FALLING BACK TOWARDS SUPPORT


    IF ( RSI(PRIME(ORDER,1),l,3) > MAXRSIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) .AND. GP(L)<-0 &
    .AND. OBVI(PRIME(ORDER,1),l) > MAXOBVIAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) .AND. &
    MAOZ(PRIME(ORDER,1),l) > MAXMAOZAVG(PRIME(ORDER,3),PRIME(ORDER,1),L,PRIME(ORDER,2)) &
    .AND. STOCKEXCHANGE /= 'SSX' .AND. STOCKEXCHANGE /= 'SZX') THEN

    DO SEARCH=1,MAXSEARCH ; IF (L+SEARCH<=NDAYS) THEN

    IF ( &
    DPV(l+SEARCH,4)/DPV(l,4) > PRIME(ORDER,7)*0.5  &
    .AND. (DPV(l+SEARCH,4) - RESISTANCELEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l+SEARCH))/&
    RESISTANCELEVEL(PRIME(ORDER,2),PRIME(ORDER,4),l+SEARCH) > 0.004*PRIME(ORDER,5) &
    .AND. GP(L+SEARCH)<-0 &
    ) THEN

!----------------------------------------------------------------------------------------------------

    ACTION(ORDER,l+SEARCH)='SELL'

!----------------------------------------------------------------------------------------------------
    ENDIF; ENDIF; ENDDO; ENDIF; ENDDO
!----------------------------------------------------------------------------------------------------
!    WRITE(*,*) 'CRITERIA EXEC SUCCESSFUL'

!    WRITE(*,*) 'RUNNING NO REPEAT'


    MAXTDAYS=PRIME(ORDER,2)*HISTORYSTEP

    IF (ORDER >= 2) THEN
    DO M=1,ORDER-1
        DO L=BEGIN+MAXTDAYS,NDAYS
            IF ( ACTION(ORDER,l) == ACTION(ORDER-M,l) )THEN
            ACTION(ORDER,l)=''
            ENDIF
        ENDDO
    ENDDO
    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------
!    WRITE(*,*) 'RUNNING PRINT FORECAST'

    CALL PRINTOPENFORECAST(CO,MAXHISTORY,MAXSEG,MAXPARTITION,HISTORYSTEP,BEGIN,NDAYS,HOLDINGDAYS,MAXTAU,&
    ORDER,PARTITION,SEG,TAU,ACTION,CODATE,DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,COMPANY,VERSION,&
    MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,MAXMAOZAVG,MINMAOZAVG,PRIME,ALLCOMPANIES,HISTORY,MAXMEMORY,&
    RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTLEVEL,SUPPORTMEAN,CALENDARLOSS,CALENDARRETURNS,I,AGENT_ID,MAXORDER)

!    CALL PRINTCLOSEFORECAST(i,CO,MAXHOLDING,NDAYS,ORDER,HOLDINGDAYS,ACTION,CODATE,PRIME,ALLCOMPANIES,DPV,MAXORDER)

!    WRITE(*,*) 'RUNNING GNUPLOTCODE'

!    IF (ORDER==1) THEN
    CALL GNUPLOTCODE(NDAYS,ORDER,TAU,HISTORY,SEG,HISTORYSTEP,MAXHISTORY,ALLCOMPANIES,CO,CODATE,MAXSEG,MAXPARTITION,&
    DPV,WDPV,RSI,OBVI,MAOZ,MA,GP,PATTERNBACKDATE,MAXRSIAVG,MINRSIAVG,MAXOBVIAVG,MINOBVIAVG,I,MAXMEMORY,&
    MAXMAOZAVG,MINMAOZAVG,RESISTANCELEVEL,RESISTANCEMEAN,SUPPORTLEVEL,SUPPORTMEAN,ACTION,PRIME,MAXTAU,MAXORDER)
!    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------
!    WRITE(*,*) ' RUNNING NUMBERS'

    WRITE(*,'(A7,1X,1X,I2,1X,A6,4X, 7(A4,1X,I2,1X))') &
    'AGENT 3', ORDER, 'PRIMES',&
    'TAU=',PRIME(ORDER,1),&
    'HIS=',PRIME(ORDER,2),&
    'SEG=',PRIME(ORDER,3),&
    'PAR=',PRIME(ORDER,4),&
    'EPS=',PRIME(ORDER,5),&
    'HOL=',PRIME(ORDER,6),&
    'DEL=',PRIME(ORDER,7)

    WRITE(*,'( F6.2,1X, I2,1X, F7.2,1X, I4,1X, 2(F10.2,1X), 2(F5.0,1X))') &
NETLOSSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
NINT(LOSERSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))),&
NETGAINA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
NINT(GAINERSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))),&
PATHCOSTA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHPROFITA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHCOSTCOUNTA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)),&
PATHPROFITCOUNTA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))

!    WRITE(*,*) ' RUNNING AVERAGES'
    !AVERAGES
    S3(0)=S3(0)+1

    S3(1)=S3(1)+PRIME(ORDER,1) ;S3(2)=S3(2)+PRIME(ORDER,2) ;S3(3)=S3(3)+PRIME(ORDER,3); S3(4)=S3(4)+PRIME(ORDER,4)
    S3(5)=S3(5)+PRIME(ORDER,5); S3(6)=S3(6)+PRIME(ORDER,6); S3(7)=S3(7)+PRIME(ORDER,7)

    S3_AVG(1)= S3(1) / REAL( S3(0) ); S3_AVG(2)= S3(2) / REAL( S3(0) ); S3_AVG(3)= S3(3) / REAL( S3(0) )
    S3_AVG(4)= S3(4) / REAL( S3(0) ); S3_AVG(5)= S3(5) / REAL( S3(0) ); S3_AVG(6)= S3(6) / REAL( S3(0) )
    S3_AVG(7)= S3(7) / REAL( S3(0) )

    GRATIO(3)= GRATIO(3) + &
    GAINERSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))&
    / ( GAINERSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))&
    + LOSERSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)))

    GRATIO_AVG(3)=GRATIO(3) / S3(0)

    NETGAIN(3)=NETGAIN(3)+&
    NETGAINA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))
    NETGAIN_AVG(3)=NETGAIN(3) / S3(0)

    NETLOSS(3)=NETLOSS(3)+&
    NETLOSSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))
    NETLOSS_AVG(3)= NETLOSS(3) / S3(0)

!------------------------------------------------------------------------------------------------------------------------------------

!    WRITE(*,*) ' RUNNING GRATIO CLEAN'

    MASTERA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7))=0


    DO HOLDINGDAYS=1,MAXHOLDING
    DO DELTA=MINDELTA,MAXDELTA
    DO EPSILON=MINEPSILON,MAXEPSILON
    DO PARTITION=1,MAXPARTITION
    DO SEG=1,MAXSEG
    DO HISTORY=1,MAXHISTORY
    DO TAU=MINTAU,MAXTAU

    IF (NETGAINA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        NETGAINA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        NETLOSSA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        NETLOSSA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        PATHCOSTA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        PATHCOSTA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) .OR. &
        PATHPROFITA3(PRIME(ORDER,1),PRIME(ORDER,2),PRIME(ORDER,3),PRIME(ORDER,4),PRIME(ORDER,5),PRIME(ORDER,6),PRIME(ORDER,7)) == &
        PATHPROFITA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA) &
        ) &


        MASTERA3(TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA)=0

    ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO;ENDDO

!------------------------------------------------------------------------------------------------------------------------------------
    ENDIF
    ENDDO !END ORDER CYCLE
!------------------------------------------------------------------------------------------------------------------------------------
























!------------------------------------------------------------------------------------------------------------------------------------

    WRITE(*,'(A2,3X,7(A8,1X,F4.1,1X))') &
    'S1','AVG_TAU=',S1_AVG(1),'AVG_HIS=',S1_AVG(2),'AVG_SEG=',S1_AVG(3),&
         'AVG_PAR=',S1_AVG(4),'AVG_EPS=',S1_AVG(5),'AVG_HOL=',S1_AVG(6) ,'AVG_DEL=',S1_AVG(7)

    WRITE(*,'(A2,3X,7(A8,1X,F4.1,1X))') &
    'S3','AVG_TAU=',S3_AVG(1),'AVG_HIS=',S3_AVG(2),'AVG_SEG=',S3_AVG(3),&
         'AVG_PAR=',S3_AVG(4),'AVG_EPS=',S3_AVG(5),'AVG_HOL=',S3_AVG(6),'AVG_DEL=',S3_AVG(7)


    WRITE(*,'(4X,4(3X,A9,2X))') 'AGENT1 S1',  'AGENT3 S1'
    WRITE(*,'(4X,4(A12,2X))') 'BUY PROGRAM','SELL PROGRAM'

    WRITE(*,'(A5,2(6X,F0.3,2X))') &
    'P(S)=', &
    GRATIO_AVG(1),     GRATIO_AVG(3)
    WRITE(*,'(A5,2(6X,F0.3,2X))') &
    'E(G)=', &
    NETGAIN_AVG(1),    NETGAIN_AVG(3)
    WRITE(*,'(A5,2(6X,F0.3,2X))') &
    'E(L)=', &
    NETLOSS_AVG(1),    NETLOSS_AVG(3)

    WRITE(*,'(A5,2(3X,F6.3,5X))') &
    'RATIO',S1(0)/ REAL( S1(0)+S2(0)+S3(0)+S4(0) ), S3(0)/ REAL( S1(0)+S2(0)+S3(0)+S4(0) )
    WRITE(*,'(A6,2(2X,F4.0,6X))') &
    'ORDERS', S1(0), S3(0)

    WRITE(*,'(29X,A19)') 'MONTHLY PERFORMANCE'
    WRITE(*,'(10X,9(A4,9X))') '2008','2009','2010','2011','2012','2013','2014','2015'

    DO k=1,12

    WRITE(*,'(A4,1X,2(4(F6.1,1X,F5.1,1X)))') MONTH(k),&
!CALENDARRETURNS(2007,k),CALENDARLOSS(2007,k),&
    CALENDARRETURNS(2008,k),CALENDARLOSS(2008,k),&
    CALENDARRETURNS(2009,k),CALENDARLOSS(2009,k),&
    CALENDARRETURNS(2010,k),CALENDARLOSS(2010,k),&
    CALENDARRETURNS(2011,k),CALENDARLOSS(2011,k),&
    CALENDARRETURNS(2012,k),CALENDARLOSS(2012,k),&
    CALENDARRETURNS(2013,k),CALENDARLOSS(2013,k),&
    CALENDARRETURNS(2014,k),CALENDARLOSS(2014,k),&
    CALENDARRETURNS(2015,k),CALENDARLOSS(2015,k)
    ENDDO


!------------------------------------------------------------------------------------------------------------------------------------

                                                        END SUBROUTINE

                                                          END MODULE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------
























!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

                                                        PROGRAM SYSTEM_V183

    USE ALLOCMOD
    USE MACXMETHODMOD
    USE RSIXMETHODMOD
    USE OBVXMETHODMOD
    USE TIDXMETHODMOD
    USE ATCXMETHODMOD
    USE SLCTMETHODMOD
    USE SUBROUTINES


    IMPLICIT NONE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

    WRITE(*,*) 'SELECT TIMEZONE'

    WRITE(*,*) '(1) UNITED STATES'
    WRITE(*,*) '(2) CANADA'
    WRITE(*,*) '(3) EUROPE (LONDON AND GERMANY)'
    WRITE(*,*) '(4) EAST ASIA & OCEANIA (CHINA, HONG KONG AND AUSTRALIA)'
    WRITE(*,*) '(5) MEXICO'

    READ(*,*) EXCHANGE

    CALL LISTGEN(ALLCOMPANIES,CO,EXCHANGE,COUNTRY,XPREDICTIONS,XOBJECTIVE, &
                XPERFORMANCE)

    CALL TODAYFOLDER(DATEFOLDER,TODAY)

    OPEN(UNIT=20,FILE='/GeTeX/system/PREDICTIONS/'//COUNTRY,STATUS='UNKNOWN',ACCESS='APPEND')
!    OPEN(UNIT=20,FILE='/GeTeX/system/PREDICTIONS/'//COUNTRY//VERSION//TRIM(DATEFOLDER),STATUS='UNKNOWN')
    OPEN(UNIT=21,FILE='/GeTeX/system/LIQUID'//COUNTRY,STATUS='UNKNOWN')
    OPEN(UNIT=25,FILE='/GeTeX/system/DEBUG/'//COUNTRY,STATUS='UNKNOWN',ACCESS='APPEND')
!    OPEN(UNIT=25,FILE='/GeTeX/system/DEBUG/'//COUNTRY//VERSION//TRIM(DATEFOLDER),STATUS='UNKNOWN')
!    OPEN(UNIT=26,FILE='/GeTeX/system/AUTOMATION/'//COUNTRY//VERSION//TRIM(DATEFOLDER),STATUS='UNKNOWN',ACCESS='APPEND')
   OPEN(UNIT=26,FILE='/GeTeX/system/AUTOMATION/'//COUNTRY//VERSION//TRIM(DATEFOLDER),STATUS='UNKNOWN')
    OPEN(UNIT=27,FILE='/GeTeX/system/LOG/'//COUNTRY,STATUS='UNKNOWN',ACCESS='APPEND')
!    OPEN(UNIT=27,FILE='/GeTeX/system/LOG/'//COUNTRY//VERSION,STATUS='UNKNOWN')
    OPEN(UNIT=28,FILE='/GeTeX/system/LEVELS/'//COUNTRY//VERSION,STATUS='UNKNOWN')
    OPEN(UNIT=100001,FILE='/GeTeX/ForecastService/'//COUNTRY//'-TABLE.tex',STATUS='UNKNOWN')
!------------------------------------------------------------------------------------------------------------------------------------

    CALL MAILINGFORECAST(COUNTRY) ! GENERATE LATEX FORMAT FORECAST SERVICE REPORT

!------------------------------------------------------------------------------------------------------------------------------------

    !UNIVERSAL PARAMENTERS
    TOTALCOUNTDAYS=0

    MAXPARTITION=7 !HOW MUCH AVERAGE TO TAKE ON SECOND CLASS METHODS.
    MAXSEG=9 !REGULATE OF WIDE OR NARROW. HOW MUCH AVERAGE TO TAKE ON FIRST CLASS METHODS.

    MAXHISTORY=3 !REGULATES HOW CLOSE THE MAX,MIN LEVELS FOLLOW THE SIGNAL LINE. ( >> 1) LINE, (<< 100) MANY FALSE SIGNALS.
    HISTORYSTEP=80 !PROPORTIONAL TO MAXHISTORY, REGULATES HOW MUCH MEMORY EACH l WITHIN EACH METHOD HAS.

    MAXMEMORY=HISTORYSTEP

    MAXHOLDING=5 !REGULATES LONG/SHORT TERM EQUITIY HOLDING CAPACITY.
    MAXSEARCH=10 !HOW FAR AWAY FROM FIRST CLASS METHODS SHOULD SEEK LOOKING FOR A TRIGGER IN SECOND CLASS METHODS.

    MINEPSILON=-5 !ERROR MARGIN FOR SECOND CLASS METHODS. LOWER BOUND.
    MAXEPSILON=5 !ERROR MARGIN FOR SECOND CLASS METHODS. HIGHER BOUND.

    MINTAU=7
    MAXTAU=21

    MAXORDER=7

    MINDELTA=-5
    MAXDELTA=5

    MINSTAT=0.85

!TAU,HISTORY,SEG,PARTITION,EPSILON,HOLDINGDAYS,DELTA

    ALLOCATE(NETGAINA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    NETLOSSA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    GAINERSA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    LOSERSA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    NETGAINA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    NETLOSSA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    GAINERSA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    LOSERSA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHCOSTA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHCOSTA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHPROFITA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHPROFITA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHPROFITCOUNTA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHPROFITCOUNTA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHCOSTCOUNTA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    PATHCOSTCOUNTA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    MASTERA1(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA),&
    MASTERA3(MINTAU:MAXTAU,MAXHISTORY,MAXSEG,MAXPARTITION,MINEPSILON:MAXEPSILON,MAXHOLDING,MINDELTA:MAXDELTA) &
    )

    ALLOCATE( S1(0:7),S2(0:7),S3(0:7),S4(0:7),S1_AVG(1:7),S2_AVG(1:7),S3_AVG(1:7),S4_AVG(1:7),&
    GRATIO_AVG(4),NETGAIN_AVG(4),NETLOSS_AVG(4),GRATIO(4),NETGAIN(4),NETLOSS(4) )

    ALLOCATE(PRIME(MAXORDER,1:7))
    PRIME=0

    S1=0.01;S2=0.01;S3=0.01;S4=0.01

    S1_AVG=0;S2_AVG=0;S3_AVG=0;S4_AVG=0
    GRATIO_AVG=0;NETGAIN_AVG=0;NETLOSS_AVG=0
    GRATIO=0;NETGAIN=0;NETLOSS=0

    ALLOCATE( CALENDARRETURNS(1998:2020,12),CALENDARLOSS(1998:2020,12) )
    !UNIVERSAL INIT
    CALENDARRETURNS=0; CALENDARLOSS=0

!------------------------------------------------------------------------------------------------------------------------------------

    !BEGIN COMPANY i CYCLE
    DO i=1,ALLCOMPANIES

    !UNIVERSAL LOCAL_INIT_TMP WITH A FRESH COPY EACH TIME
!    PRIME=0

!------------------------------------------------------------------------------------------------------------------------------------
    CALL SYSTEM_CLOCK(COUNT1, COUNT_RATE)

    READ(CO(i),'(A3,A12)') STOCKEXCHANGE,COMPANY ; READ(COMPANY,'(A1,A11)') FIRSTLETTER,LASTLETTER
    IF (FIRSTLETTER == '^') THEN ; COMPANY='IDX'//LASTLETTER ;    ENDIF

    WRITE(*,'(A8,1X,A7,1X,A2,1X,A3,1X,A3,A14,1X,F7.2,A1)') &
    'COMPANY:',COMPANY,'X:',STOCKEXCHANGE,' | ','ANALYZING ... ',i/REAL(ALLCOMPANIES)*100,'%'

!------------------------------------------------------------------------------------------------------------------------------------
    !STOCK MARKET DATA FOR COMPANY i FILE
    OPEN(UNIT=(i+list1),FILE='/GeTeX/system/tickers/'//COUNTRY//'/'//TRIM(CO(i)),STATUS='OLD')
    OPEN(UNIT=(i+list2),FILE='/GeTeX/system/EYE/'//COUNTRY//'/'//TRIM(CO(i)),STATUS='UNKNOWN')
    OPEN(UNIT=(i+list3),FILE='/GeTeX/system/EYE/'//COUNTRY//'/' //'GNU'//TRIM(CO(i)),STATUS='UNKNOWN')
!------------------------------------------------------------------------------------------------------------------------------------

    !NDAYS TOTAL NUMBER GENERATION

    CALL counter(i,NDAYS)

    TOTALCOUNTDAYS=NDAYS+TOTALCOUNTDAYS
    BEGIN=1; IF (BEGIN < 0) THEN; BEGIN=1;    ENDIF

    IF ( NDAYS < 2*(MAXHISTORY*HISTORYSTEP)+350 ) THEN
    CALL closefile(i)
    CYCLE !END COMPANY i CYCLE
    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------
    ALLOCATE(DPV(0:NDAYS,1:6), WDPV(0:NDAYS,1:6), CODATE(0:NDAYS))
    ALLOCATE(RSI(MAXTAU,0:NDAYS,1:4),MAXRSIAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY), MINRSIAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY))
    ALLOCATE(MAOZ(MAXTAU,0:NDAYS),MAXMAOZAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),MINMAOZAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY))
    ALLOCATE(OBVI(MAXTAU,0:NDAYS),MAXOBVIAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY),MINOBVIAVG(MAXSEG,MAXTAU,NDAYS,MAXHISTORY))
    ALLOCATE(MA(2:200,0:NDAYS,1:4))
    ALLOCATE(GP(NDAYS), PATTERNBACKDATE(MAXMEMORY,NDAYS,1:2))
    ALLOCATE(ACTION(MAXORDER,NDAYS))
    ACTION=''

    ALLOCATE(RESISTANCEMEAN(MAXHISTORY,MAXPARTITION,NDAYS),RESISTANCELEVEL(MAXHISTORY,MAXPARTITION,NDAYS),&
             SUPPORTMEAN(MAXHISTORY,MAXPARTITION,NDAYS),SUPPORTLEVEL(MAXHISTORY,MAXPARTITION,NDAYS))
!------------------------------------------------------------------------------------------------------------------------------------
    CALL INTERNALDATAFEED(i,NDAYS,WDPV,CODATE,BEGIN)
!------------------------------------------------------------------------------------------------------------------------------------
    CALL EXTERNALDATAFEED(i,NDAYS,DPV,CODATE,BEGIN)
!------------------------------------------------------------------------------------------------------------------------------------


!CRITERIA TO DISREGARD NON-LIQUID STOCKS

    AvgMoney=SUM(DPV(NDAYS-500:NDAYS,4)*DPV(NDAYS-500:NDAYS,6))/REAL(NDAYS-500)
    avgPRICE=SUM(DPV(NDAYS-500:NDAYS,4))/REAL(NDAYS-500)

    IF (EXCHANGE==1 .OR. EXCHANGE==2 .OR. EXCHANGE==3 .OR. EXCHANGE==5) THEN
    MINCAP=375000 ; MINAVGPRICE=10
    ELSE IF (EXCHANGE==4) THEN
    MINCAP=275000 ; MINAVGPRICE=2
    ENDIF

!    IF (AvgMoney < MINCAP .OR. avgPRICE < MINAVGPRICE .OR. DPV(NDAYS,4) < MINAVGPRICE .OR. NDAYS < 1250) THEN
    IF (AvgMoney < MINCAP .OR. DPV(NDAYS,4) < MINAVGPRICE ) THEN
    CALL closefile(i)
    DEALLOCATE( DPV,WDPV,CODATE,RSI,MAXRSIAVG,MINRSIAVG,MAOZ,MINMAOZAVG,MAXMAOZAVG,OBVI,MINOBVIAVG,MAXOBVIAVG,MA,GP,&
    PATTERNBACKDATE,RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL,ACTION)
    CYCLE !END COMPANY i CYCLE
    ENDIF

!------------------------------------------------------------------------------------------------------------------------------------

    !LIQUID STOCKS
    WRITE(21,*) CO(i)

!------------------------------------------------------------------------------------------------------------------------------------

    CALL EXPAV(NDAYS,DPV,MA,BEGIN)

!   CALL SMAV(NDAYS,DPV,MA,MU,BEGIN)

    CALL SYSTEM_CLOCK(COUNT2, COUNT_RATE)
    WRITE(*,*) 'NUMX COMPLETE', (COUNT2 - COUNT1) / REAL(COUNT_RATE)

!------------------------------------------------------------------------------------------------------------------------------------


    CALL TIDXMETHOD(BEGIN,NDAYS,DPV,RESISTANCEMEAN,RESISTANCELEVEL,&
    SUPPORTMEAN,SUPPORTLEVEL,MAXPARTITION,MAXHISTORY,HISTORYSTEP)

    CALL SYSTEM_CLOCK(COUNT3,COUNT_RATE)
    WRITE(*,*) 'TIDX METHOD COMPLETE', (COUNT3 - COUNT2) / REAL(COUNT_RATE)

!------------------------------------------------------------------------------------------------------------------------------------

    CALL MACXMETHOD(NDAYS,BEGIN,DPV,MAOZ,MAXSEG,MAXHISTORY,MAXMAOZAVG,MINMAOZAVG,HISTORYSTEP,MINTAU,MAXTAU)

    CALL SYSTEM_CLOCK(COUNT4, COUNT_RATE)
    WRITE(*,*) 'MACX METHOD COMPLETE', (COUNT4 - COUNT3) / REAL(COUNT_RATE)

    CALL OBVXMETHOD(BEGIN,NDAYS,DPV,OBVI,MAXSEG,MAXHISTORY,MAXOBVIAVG,MINOBVIAVG,HISTORYSTEP,MINTAU,MAXTAU)

    CALL SYSTEM_CLOCK(COUNT5, COUNT_RATE)
    WRITE(*,*) 'OBVX METHOD COMPLETE', (COUNT5 - COUNT4) / REAL(COUNT_RATE)

    CALL RSIXMETHOD(NDAYS,RSI,BEGIN,MAXSEG,MAXHISTORY,MAXRSIAVG,MINRSIAVG,HISTORYSTEP,DPV,MINTAU,MAXTAU)

    CALL SYSTEM_CLOCK(COUNT6, COUNT_RATE)
    WRITE(*,*) 'RSIX METHOD COMPLETE', (COUNT6 - COUNT5) / REAL(COUNT_RATE)

!------------------------------------------------------------------------------------------------------------------------------------

    CALL ATCXMETHOD(BEGIN,NDAYS,DPV,CODATE,GP,PATTERNBACKDATE,MAXHOLDING,MAXHISTORY,HISTORYSTEP,MAXMEMORY)

    CALL SYSTEM_CLOCK(COUNT7, COUNT_RATE)
    WRITE(*,*) 'ATCX METHOD COMPLETE', (COUNT7 - COUNT6) / REAL(COUNT_RATE)
!------------------------------------------------------------------------------------------------------------------------------------

!   PARAMETERS SELECTOR METHOD

    CALL SLCTMETHOD(NDAYS,RSI,DPV,ALLCOMPANIES,CO,COMPANY,I,MAXMEMORY,MAXORDER,&
    MAXRSIAVG,MINRSIAVG,BEGIN,MA,RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL,&
    STOCKEXCHANGE,CODATE,MAXPARTITION,MAXHISTORY,GP,HISTORYSTEP,PATTERNBACKDATE,PRIME,VERSION, &
    MAXOBVIAVG,MINOBVIAVG,OBVI,MAXSEG,MAXHOLDING,MAOZ,MAXMAOZAVG,MINMAOZAVG,WDPV,MAXSEARCH,&
    MINDELTA,MAXDELTA,MINSTAT,&
    S1,S2,S3,S4,S1_AVG,S2_AVG,S3_AVG,S4_AVG,GRATIO_AVG,NETGAIN_AVG,NETLOSS_AVG,MAXEPSILON,&
    CALENDARRETURNS,CALENDARLOSS,MAXTAU,MINTAU,GRATIO,NETGAIN,NETLOSS,AGENT_ID,MINEPSILON,&
    NETGAINA1,NETLOSSA1,GAINERSA1,LOSERSA1,NETGAINA3,NETLOSSA3,GAINERSA3,LOSERSA3,&
    PATHCOSTA1,PATHCOSTA3,PATHPROFITA1,PATHPROFITA3,PATHPROFITCOUNTA1,PATHPROFITCOUNTA3, &
    PATHCOSTCOUNTA1,PATHCOSTCOUNTA3,MASTERA1,MASTERA3)


    CALL SYSTEM_CLOCK(COUNT6, COUNT_RATE)
    WRITE(*,*) 'SLCT COMPLETE', (COUNT6 - COUNT5) / REAL(COUNT_RATE)

!------------------------------------------------------------------------------------------------------------------------------------




!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

!                                             OUTSIDE OF TAU CYCLE - INSIDE COMPANY CYCLE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------




!------------------------------------------------------------------------------------------------------------------------------------

!                                                       PRINT SCREEN SUMMARY

    WRITE(*,'(A12,1X,A7,1X,A2,1X,A3,1X,A3,1X,A10,F7.2,A1)')'COMPANY:',&
                        COMPANY,'X:',STOCKEXCHANGE,' | ','COMPLETED ',i/REAL(ALLCOMPANIES)*100,'%'
    WRITE(*,*) '----------------------------------------------'

!--------------------------------------------------------------------------------------------------------------------------

    CALL closefile(i)

    DEALLOCATE( DPV,WDPV,CODATE,RSI,MAXRSIAVG,MINRSIAVG,MAOZ,MINMAOZAVG,MAXMAOZAVG,OBVI,MINOBVIAVG,MAXOBVIAVG,MA,GP,&
    PATTERNBACKDATE,RESISTANCEMEAN,RESISTANCELEVEL,SUPPORTMEAN,SUPPORTLEVEL,ACTION)

!------------------------------------------------------------------------------------------------------------------------------------

                                                    ENDDO !COMPANY CYCLE

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

!                                                       OUTSIDE OF i

!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------

!       AVERAGE HISTORICAL DAYS OF DATA FOR ALL COMPANIES
    AVGNDAYS=TOTALCOUNTDAYS/REAL(ALLCOMPANIES)

    WRITE(*,*) 'AVERAGE HISTORICAL DATA:'
    WRITE(*,*) 'DAYS=',AVGNDAYS,'MONTHS=',AVGNDAYS/30.0
    WRITE(*,*) 'YEARS=',(AVGNDAYS/20.0)/12.0

!------------------------------------------------------------------------------------------------------------------------------------

    WRITE(100001,*) '\end{longtable}'

!--------------------------------------------------END PROGRAM MAIN CONTROL----------------------------------------------------------


    DEALLOCATE(NETGAINA1,NETLOSSA1,GAINERSA1,LOSERSA1, NETGAINA3,NETLOSSA3,GAINERSA3,LOSERSA3, &
    PATHCOSTA1,PATHCOSTA3,PATHPROFITA1,PATHPROFITA3,PATHPROFITCOUNTA1,PATHPROFITCOUNTA3, &
    PATHCOSTCOUNTA1,PATHCOSTCOUNTA3,PRIME)

    DEALLOCATE (S1,S2,S3,S4,S1_AVG,S2_AVG,S3_AVG,S4_AVG,&
    GRATIO_AVG,NETGAIN_AVG,NETLOSS_AVG,GRATIO,NETGAIN,NETLOSS)

    DEALLOCATE(CALENDARLOSS,CALENDARRETURNS)
!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------



                                                          END PROGRAM



!------------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------------------------
